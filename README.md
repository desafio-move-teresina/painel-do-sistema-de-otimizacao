# Optime

Desenvolvimento **frontend** da aplicação de gerenciamento das empresas, ônibus, mapas, pontos de ônibus e rotas para a prefeitura de Teresina.

As tecnologias usadas são [TypeScript](https://www.typescriptlang.org), [React](https://reactjs.org), [Next.js](https://nextjs.org) e [@react-google-maps/api](https://www.npmjs.com/package/@react-google-maps/api) para desenvolver os mapas.

## Links

### Projeto

- [Entidades e Rotas](https://optime.atlassian.net/wiki/spaces/OPT/pages/289144834/Documenta+o+-+Entidades+e+Rotas) - documentação inicial da API
- [Layout](https://www.figma.com/file/v9hpRn7st0zjZotDhyEq2t/Figma-Admin-Dashboard-UI-Kit-Community?node-id=0%3A1) - layout do projeto no Figma
- [Bitbucket](https://bitbucket.org/desafio-move-teresina/painel-do-sistema-de-otimizacao/src/master) - repositório do projeto no Bitbucket
- [Jira](https://optime.atlassian.net/jira/software/projects/OPT/boards/1) - gerenciamento do projeto no Jira
- [Confluence](https://optime.atlassian.net/wiki/spaces/OPT/overview) - documentação do projeto no Confluence

### Docs e outros

- [Google Cloud Console](https://console.cloud.google.com/home/dashboard) - gerar chaves para API do Google Maps em _APIs e serviços_ > _Credenciais_
- [TypeScript](https://www.typescriptlang.org/docs/handbook/intro.html) - TypeScript Handbook
- [React](https://reactjs.org/docs/react-api.html) - documentação do React
- [Next.js](https://nextjs.org/docs) - documentação do Next.js
- [Google Maps API](https://developers.google.com/maps/documentation/javascript/reference/polygon#Polygon) - documentação do Google Maps JavaScript API
- [@react-google-maps/api](https://www.npmjs.com/package/@react-google-maps/api) - pacote no NPM
- [Docs do @react-google-maps/api](https://react-google-maps-api-docs.netlify.app) - documentação do pacote _@react-google-maps/api_
- [How to edit a polygon](https://stackoverflow.com/questions/20789385/how-can-i-detect-when-an-editable-polygon-is-modified) - como editar um polígono no Stack Overflow
- [Polyline utility](https://developers.google.com/maps/documentation/utilities/polylineutility) - codificar e decodificar polylines

## Variáveis de ambiente

Antes de executar o projeto é necessário configurar algumas variáveis de ambientes que não estão versionadas no Git. Para isso abra o arquivo `.env.local.example` e siga as instruções.

## Executar o projeto usando o Docker

Na pasta raiz do projeto execute:

```bash
docker compose up -d
```

> A flag `-d` (detach) é opcional: não conecta o terminal ao processo do container.

O servidor web será iniciado em [http://localhost:3003](http://localhost:3003).

Para parar todos os containers execute:

```bash
docker compose stop
```

## Configurar e executar o projeto localmente

Primeiro instale as dependências:

```bash
yarn
```

Depois, execute o projeto:

```bash
yarn dev
```

O servidor web será iniciado em [http://localhost:3003](http://localhost:3003).

## Build

Para fazer o build e executar o projeto na versão de produção execute:

```bash
# Compila o projeto
yarn build

# Roda a versão de compilada do projeto
yarn start
```

Da mesma forma, o servidor web será iniciado em [http://localhost:3003](http://localhost:3003). A versão de produção é bem mais rápida e mais leve. Além disso ela não dá refresh esporádicos como a versão de desenvolvimento.

## Extensões

Instale também as seguintes extensões no Visual Studio code:

- [Debugger for Chrome](https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-chrome) - integrar debug entre o Chrome e o vscode
- [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) - linting
- [Prettier - Code formatter](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode) - formatação do código
- [Code Spell Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker) - verificação ortográfica
- [Portuguese - Code Spell Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker-portuguese) - verificação ortográfica em português

Para executar o ESLint:

```bash
yarn lint
```

> Corrija os erros reportados pelo ESLint antes de fazer commit.

## Debug

Execute o projeto usando `yarn dev` ou `docker compose up -d`. Quando o servidor estiver rodando, no vscode, aperte _F5_ para iniciar o debug. Será possível adicionar break points, ver valor das variáveis etc.

## Versões

O projeto está sendo testado usando ferramentas nas seguintes versões:

```bash
node -v
v14.16.1

npm -v
6.14.12

yarn -v
1.22.0

docker -v
Docker version 20.10.7, build f0df350
```

## Galeria

![Map editor](https://user-images.githubusercontent.com/1520962/128871201-3b984899-8086-4b97-b535-63c03e93ad4a.png)

![Bus stop info](https://user-images.githubusercontent.com/1520962/128871647-e0a204c4-c339-4acd-ba7d-36dafc6bf7e8.png)
