import { useEffect } from 'react'
import { useQuery, useQueryClient } from 'react-query'
import { SubmitHandler, useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup/dist/yup'
import * as yup from 'yup'
import { toast } from 'react-toastify'

import { decisionVariablesRepository } from 'repositories'
import { defaultIntegerRule } from 'utils/validation'
import { PageLayout } from 'components/PageLayout'
import { informQueryStatus } from 'utils/informQueryStatus'
import styles from './styles.module.scss'

type SettingsFormData = {
  stopMinimumDistance: number
  stopMaximumDistance: number
  maximumWait: number
  defaultBusStopRadius: number
}

const settingsFormSchema = yup.object().shape({
  stopMinimumDistance: defaultIntegerRule,
  stopMaximumDistance: defaultIntegerRule,
  maximumWait: defaultIntegerRule,
  defaultBusStopRadius: defaultIntegerRule,
})

export default function Settings() {
  const query = useQuery('settings', decisionVariablesRepository.get)

  const queryClient = useQueryClient()

  const { register, handleSubmit, formState, reset, setValue } =
    useForm<SettingsFormData>({
      resolver: yupResolver(settingsFormSchema),
    })

  const onSubmit: SubmitHandler<SettingsFormData> = async (values) => {
    try {
      const newData = await decisionVariablesRepository.create(values)
      queryClient.setQueryData('settings', newData)
      toast.success('Campos atualizados')
      reset()
    } catch (error) {
      toast.error('Erro ao atualizar')
    }
  }

  const { errors } = formState

  const { status, data } = query

  useEffect(() => {
    informQueryStatus(status)

    if (data) {
      const formStateClean = !formState.isDirty
      if (formStateClean) {
        setValue('stopMinimumDistance', data.stopMinimumDistance)
        setValue('stopMaximumDistance', data.stopMaximumDistance)
        setValue('maximumWait', data.maximumWait)
        setValue('defaultBusStopRadius', data.defaultBusStopRadius)
      }
    }
  }, [status, data, formState.isDirty, setValue])

  return (
    <PageLayout headTitle="Configurações | Optime" title="Configurações">
      <form
        className={styles.decisionVariables}
        onSubmit={handleSubmit(onSubmit)}
      >
        <h2>Variáveis de decisão</h2>

        <fieldset>
          <h3>Distância entre pontos de parada</h3>
          <div>
            <input
              type="number"
              placeholder="Mínimo"
              title="Distância mínima entre pontos de parada em metros"
              {...register('stopMinimumDistance')}
            />
            <span>(em metros)</span>
            <label>{errors.stopMinimumDistance?.message}</label>
          </div>

          <div>
            <input
              type="number"
              placeholder="Máximo"
              title="Distância máxima entre pontos de parada em metros"
              {...register('stopMaximumDistance')}
            />
            <span>(em metros)</span>
            <label>{errors.stopMaximumDistance?.message}</label>
          </div>
        </fieldset>

        <fieldset>
          <h3>Tempo de espera do passageiro</h3>
          <div>
            <input
              type="number"
              placeholder="Máximo"
              title="Tempo de espera máximo do passageiro em minutos"
              {...register('maximumWait')}
            />
            <span>(em minutos)</span>
            <label>{errors.maximumWait?.message}</label>
          </div>
        </fieldset>

        <fieldset>
          <h3>Raio de abrangência de um ponto de parada</h3>
          <div>
            <input
              type="number"
              placeholder="Raio"
              title="Raio de abrangência de um ponto de parada em metros"
              {...register('defaultBusStopRadius')}
            />
            <span>(em metros)</span>
            <label>{errors.defaultBusStopRadius?.message}</label>
          </div>
        </fieldset>

        <div className={styles.buttonContainer}>
          <button
            type="submit"
            disabled={formState.isSubmitting || status === 'loading'}
          >
            Salvar
          </button>
        </div>
      </form>
    </PageLayout>
  )
}
