import { useState, useEffect, ChangeEvent } from 'react'
import { useQuery } from 'react-query'
import { GoogleMap, LoadScript } from '@react-google-maps/api'
import { AiFillPrinter as PrintIcon } from 'react-icons/ai'

import { routesRepository } from 'repositories'
import { IRouteDTO, getSteps } from 'repositories/IRoutesRepository'
import { PageLayout } from 'components/PageLayout'
import { parseRoutesFromAPI } from 'utils/mapParser'
import * as mapUtils from 'utils/mapUtils'
import { BusStopMenuItem, RouteMenuItem } from 'utils/itemsController'
import { informQueryStatus } from 'utils/informQueryStatus'

import styles from './styles.module.scss'

export default function RouteReports() {
  /*
   * API calls
   */
  const query = useQuery(
    'routes',
    async () => {
      const responses = await Promise.all([
        routesRepository.list(),
        routesRepository.optimizations(),
      ])

      const [apiRoutes, apiOptimizations] = responses

      return { apiRoutes, apiOptimizations }
    },
    { refetchOnWindowFocus: false, cacheTime: 0 }
  )

  /*
   * API Data state
   */
  const { status, data } = query

  useEffect(() => {
    informQueryStatus(status)
  }, [status])

  /*
   * Component state
   */

  /* Google map's map reference */
  const [map, setMap] = useState<google.maps.Map | null>(null)

  /* The selected route. If none it's null */
  const [selectedRoute, setSelectedRoute] = useState<RouteMenuItem | null>(null)

  /* The selected API route. If none it's null */
  const [selectedAPIRoute, setSelectedAPIRoute] = useState<IRouteDTO | null>(
    null
  )

  /* Optimizations */
  const optimizations = data?.apiOptimizations ? data.apiOptimizations : []

  /* Bus stops menu items */
  const [busStopMenuItems, setBusStopMenuItems] = useState<BusStopMenuItem[]>(
    []
  )

  /* Route menu items */
  const [routeMenuItems, setRouteMenuItems] = useState<RouteMenuItem[]>([])

  /* Selected optimization */
  const [optimizationSelectValue, setOptimizationSelectValue] = useState('all')

  /*
   * Parsing API data
   */
  useEffect(() => {
    if (map && data?.apiRoutes) {
      const { apiRoutes } = data

      const { routeMenuItems, busStopMenuItems } = parseRoutesFromAPI(
        map,
        apiRoutes
      )

      setBusStopMenuItems(busStopMenuItems)
      setRouteMenuItems(routeMenuItems)

      // Resetting type of all bus stops menu items
      busStopMenuItems.forEach((busStopItem) => {
        busStopItem.type = 'hidden'
      })
    }
  }, [map, data])

  /*
   * Component state functions
   */

  /** Changes the selected route in the page  */
  const changeSelectedRoute = (route: RouteMenuItem | null) => {
    // Resetting type of all bus stops menu items
    busStopMenuItems.forEach((busStopItem) => {
      busStopItem.type = 'hidden'
    })

    // Resetting editable of all route menu items
    routeMenuItems.forEach((routeItem) => {
      routeItem.editable = false
    })

    if (route) {
      // The selected route is called editable internally, but in
      // reality it's only selected, not editable
      route.editable = true
      route.setBusStopTypes()
    }
    setSelectedRoute(route)
  }

  const onChangeSelectedOptimization = (
    event: ChangeEvent<HTMLSelectElement>
  ) => {
    const selected: string = event.target.value

    if (selected !== optimizationSelectValue) {
      changeSelectedRoute(null)
      setSelectedAPIRoute(null)
      setOptimizationSelectValue(selected)
    }
  }

  const onChangeSelectedRoute = (event: ChangeEvent<HTMLSelectElement>) => {
    const selected: string = event.target.value

    if (selected === 'none') {
      changeSelectedRoute(null)
      setSelectedAPIRoute(null)
      return
    }

    const route = routeMenuItems.find((route) => route.id === selected)
    const apiRoute = data?.apiRoutes.find(
      (apiRoute) => apiRoute.id === selected
    )
    changeSelectedRoute(route || null)
    setSelectedAPIRoute(apiRoute || null)
  }

  /**
   * On google map load complete callback
   */
  const onMapLoad = (mapRef: google.maps.Map) => {
    setMap(mapRef)

    // The SVG path is 14 x 16px, anchoring half of this to center the marker
    const point = new google.maps.Point(7, 8)
    mapUtils.svgMarker.anchor = point
    mapUtils.svgMarkerInactive.anchor = point
    mapUtils.svgMarkerRoute.anchor = point
  }

  // Steps
  const steps = selectedAPIRoute ? getSteps(selectedAPIRoute) : null

  // Routes filtered by the selected optimization
  const filteredRouteItems = routeMenuItems.filter(
    (routeItem) =>
      optimizationSelectValue === 'all' ||
      routeItem.optimizationId === optimizationSelectValue
  )

  /*
   * Component structure
   */
  return (
    <PageLayout
      headTitle="Relatórios | Optime"
      title="Relatórios"
      className={styles.container}
    >
      <header>
        <div>
          <span>
            <label>Mapa:</label>

            <select
              value={optimizationSelectValue}
              onChange={onChangeSelectedOptimization}
            >
              <option value="all">Todos</option>
              {optimizations.map((optimization) => (
                <option key={optimization.id} value={optimization.id}>
                  {optimization.name}
                </option>
              ))}
            </select>
          </span>

          <span>
            <label>Rota:</label>

            <select
              value={selectedRoute?.id || 'none'}
              onChange={onChangeSelectedRoute}
            >
              <option value="none">Selecionar</option>
              {filteredRouteItems.map((routeItem) => (
                <option key={routeItem.id} value={routeItem.id}>
                  {routeItem.name}
                </option>
              ))}
            </select>
          </span>
        </div>

        <button
          type="button"
          onClick={() => {
            print()
          }}
        >
          <PrintIcon />
        </button>
      </header>

      <LoadScript
        googleMapsApiKey={process.env.NEXT_PUBLIC_GOOGLE_MAPS_API_KEY || ''}
        libraries={mapUtils.mapLibraries}
      >
        <GoogleMap
          id="map-editor"
          onLoad={onMapLoad}
          options={mapUtils.mapOptions}
          mapContainerStyle={mapUtils.mapContainerStyleReports}
          center={mapUtils.mapCenter}
          zoom={mapUtils.mapZoom}
        />
      </LoadScript>

      {selectedAPIRoute && (
        <section className={styles.content}>
          <h3>{selectedAPIRoute.name}</h3>

          <div className={styles.grid}>
            <div>
              <h4>Resumo da Rota</h4>

              <p>
                Média de passageiros:{' '}
                <b>{selectedAPIRoute.estimatedPassengers}</b>
              </p>

              <p>
                Distância percorrida:{' '}
                <b>
                  {Math.round(selectedAPIRoute.estimatedDistance / 1000)} km
                </b>
              </p>

              <p>
                Tempo médio:{' '}
                <b>{Math.round(selectedAPIRoute.estimatedDuration / 60)} min</b>
              </p>
            </div>

            <div>
              <h4>Veículos</h4>

              <p>
                Quantidade: <b>{selectedAPIRoute.routeVehicles.length}</b>
              </p>

              <p>
                Capacidade total:{' '}
                <b>
                  {selectedAPIRoute.routeVehicles.reduce(
                    (capacity, { totalCapacity }) => capacity + totalCapacity,
                    0
                  )}
                </b>
              </p>
            </div>
          </div>

          <h3>Instruções para o motorista</h3>

          {steps?.map((step) => (
            <p
              key={step.id}
              // Warning: this can lead to HTML injection if somehow the API
              // returns unwanted HTML code or script tags
              dangerouslySetInnerHTML={{ __html: step.htmlInstruction }}
            />
          ))}
        </section>
      )}
    </PageLayout>
  )
}
