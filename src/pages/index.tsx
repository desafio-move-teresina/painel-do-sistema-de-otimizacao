import { PageLayout } from 'components/PageLayout'

export default function Home() {
  return (
    <PageLayout headTitle="Dashboard | Optime" title="Dashboard">
      <strong>Desenhe os mapas e visualize as rotas.</strong>

      <p>👏 Olá, seja bem vindo!</p>

      <strong>O texto da página é apenas um placeholder...</strong>

      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque quis
        itaque cupiditate perspiciatis id adipisci dicta fugit laborum ipsum
        qui, at placeat sint minus beatae hic modi magnam odit quam.
      </p>
      <p>
        Nobis voluptates soluta id minus? Asperiores assumenda quibusdam
        inventore et id quidem possimus libero vitae ex commodi ipsa deserunt
        deleniti in eveniet debitis aperiam nam, nisi excepturi maxime? Vel,
        omnis?
      </p>
      <p>
        Consectetur similique commodi laboriosam, aperiam nihil ipsa error est
        quidem corporis, ipsum sunt! Iste ipsam officia nisi, quas fugiat omnis
        rerum consectetur aliquam nihil est ducimus culpa mollitia libero
        blanditiis!
      </p>
      <p>
        Eveniet illum eaque magnam officia esse labore dolores maxime dolore
        consectetur tempora. Facilis cumque, deserunt cupiditate aperiam
        suscipit earum dolor eius, quasi, dignissimos illum quod! Labore sed
        earum eveniet tenetur!
      </p>
      <p>
        Quidem, quis? Labore repudiandae, nam voluptates magnam officia ducimus
        totam iure distinctio. Reprehenderit, sit exercitationem. Aliquam beatae
        assumenda excepturi corrupti maxime tempora, earum dolorum, quasi dicta
        quae eaque aperiam dignissimos?
      </p>
      <p>
        Minus debitis nobis est aspernatur. Eveniet blanditiis natus, porro aut,
        eos nesciunt necessitatibus minima sunt, culpa ab eius? Ducimus placeat
        consequuntur nam sint nostrum at facere similique dolorum unde fugiat?
      </p>
      <p>
        Explicabo error praesentium qui iure deserunt corrupti nemo ducimus
        deleniti quidem illum, officiis harum, similique beatae perspiciatis
        sequi libero maxime? A ex, incidunt similique illum vitae rem ut
        perferendis tempore!
      </p>
      <p>
        Blanditiis maxime quibusdam ullam quas culpa. Quia commodi eveniet eaque
        mollitia autem soluta, atque velit nam reprehenderit nulla ratione ea
        aut ipsum ducimus qui consequuntur vel dignissimos id iste accusantium.
      </p>
    </PageLayout>
  )
}
