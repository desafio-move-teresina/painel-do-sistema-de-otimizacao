import { useEffect } from 'react'
import { useRouter } from 'next/router'
import { useQuery } from 'react-query'
import { toast } from 'react-toastify'

import { vehiclesRepository } from 'repositories'
import { PageLayout } from 'components/PageLayout'
import { IVehicleCreateDTO } from 'repositories/IVehiclesRepository'
import { VehicleForm } from 'components/VehicleForm'
import { informQueryStatus } from 'utils/informQueryStatus'

export default function EditVehicle() {
  const router = useRouter()
  const { id: vehicleId } = router.query

  const query = useQuery(
    ['vehicle', vehicleId],
    () => vehiclesRepository.get(vehicleId as string),
    { enabled: !!vehicleId && typeof vehicleId === 'string' }
  )

  const { status, data } = query

  useEffect(() => {
    informQueryStatus(status)
  }, [status])

  const onSubmit = async (values: IVehicleCreateDTO): Promise<void> => {
    try {
      if (typeof vehicleId !== 'string') {
        throw new Error('No vehicle id')
      }
      const vehicle = { id: vehicleId, ...values }
      await vehiclesRepository.edit(vehicleId, vehicle)
      toast.success('Veículo atualizado')
      router.push('/vehicles')
    } catch (error) {
      toast.error('Erro ao atualizar')
    }
  }

  return (
    <PageLayout headTitle="Editar Veículo | Optime" title="Veículo">
      <VehicleForm title="Editar Veículo" vehicle={data} onSubmit={onSubmit} />
    </PageLayout>
  )
}
