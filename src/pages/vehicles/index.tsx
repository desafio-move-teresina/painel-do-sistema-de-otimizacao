import { useState, useEffect, ChangeEvent } from 'react'
import { useRouter } from 'next/router'
import { useQuery, useQueryClient } from 'react-query'
import { RiFilterOffFill as ClearFilterIcon } from 'react-icons/ri'
import { IoMdAddCircle as AddIcon } from 'react-icons/io'
import { MdModeEdit as EditIcon } from 'react-icons/md'
import { MdDelete as DeleteIcon } from 'react-icons/md'
import { toast } from 'react-toastify'

import { vehiclesRepository, companiesRepository } from 'repositories'
import { vehicleStatusText } from 'repositories/IVehiclesRepository'
import { PageLayout } from 'components/PageLayout'
import { informQueryStatus } from 'utils/informQueryStatus'
import styles from './styles.module.scss'

export default function Vehicles() {
  const router = useRouter()

  const [companyId, setCompanyId] = useState('')
  const [vehicleStatus, setVehicleStatus] = useState('')
  const [clearQuery, setClearQuery] = useState(false)

  const query = useQuery(['vehicles', companyId, vehicleStatus], () =>
    vehiclesRepository.list(companyId, vehicleStatus)
  )

  const queryClient = useQueryClient()

  const { status, data } = query

  // Fetch companies for the companies filter
  const companiesQuery = useQuery('companies', companiesRepository.list)
  const { data: companies } = companiesQuery

  useEffect(() => {
    informQueryStatus(status)
  }, [status])

  // Using query parameters to avoid losing state when the user
  // reloads the page or leave than press the back button
  useEffect(() => {
    if (clearQuery) {
      setClearQuery(false)
      return
    }

    const q = router.query

    // Sync companyId if it's set only in the query param
    if (q.companyId && typeof q.companyId === 'string' && !companyId) {
      setCompanyId(q.companyId)
    }

    // Sync vehicleStatus if it's set only in the query param
    if (
      q.vehicleStatus &&
      typeof q.vehicleStatus === 'string' &&
      !vehicleStatus
    ) {
      setVehicleStatus(q.vehicleStatus)
    }
  }, [clearQuery, router, companyId, vehicleStatus])

  const deleteVehicle = async (vehicleId: string) => {
    try {
      if (!data) {
        return
      }

      await vehiclesRepository.delete(vehicleId)
      const newVehicles = data.filter((vehicle) => vehicle.id !== vehicleId)
      queryClient.setQueryData(
        ['vehicles', companyId, vehicleStatus],
        newVehicles
      )
    } catch (error) {
      toast.error('Erro ao excluir')
    }
  }

  const onCompanyFilterChange = (event: ChangeEvent<HTMLSelectElement>) => {
    const companyId: string = event.target.value
    setCompanyId(companyId)

    const query = { companyId, vehicleStatus }
    router.push('/vehicles', { query })
  }

  const onVehicleStatusFilterChange = (
    event: ChangeEvent<HTMLSelectElement>
  ) => {
    const vehicleStatus: string = event.target.value
    setVehicleStatus(vehicleStatus)

    const query = { companyId, vehicleStatus }
    router.push('/vehicles', { query })
  }

  const onFilterClear = () => {
    setCompanyId('')
    setVehicleStatus('')
    setClearQuery(true)
    router.push('/vehicles')
  }

  return (
    <PageLayout
      headTitle="Veículos | Optime"
      title="Veículos"
      className={styles.container}
    >
      <header>
        <div>
          <select
            value={companyId ? companyId : ''}
            onChange={onCompanyFilterChange}
          >
            <option value="">Empresa</option>
            {companies &&
              companies.map((company) => (
                <option key={company.id} value={company.id}>
                  {company.name}
                </option>
              ))}
          </select>

          <select
            value={vehicleStatus ? vehicleStatus : ''}
            onChange={onVehicleStatusFilterChange}
          >
            <option value="">Situação</option>
            <option value="active">Ativo</option>
            <option value="sold">Vendido</option>
            <option value="maintenance">Manutenção</option>
          </select>

          <button type="button" onClick={onFilterClear}>
            <ClearFilterIcon />
          </button>
        </div>

        <button type="button" onClick={() => router.push('/vehicles/create')}>
          <AddIcon />
        </button>
      </header>

      <table>
        <thead>
          <tr>
            <th>Placa</th>
            <th>Empresa</th>
            <th>Capacidade</th>
            <th>Situação</th>
          </tr>
        </thead>

        <tbody>
          {data &&
            data.map((vehicle) => (
              <tr
                key={vehicle.id}
                onClick={() => router.push(`/vehicles/${vehicle.id}`)}
              >
                <td>{vehicle.licencePlate}</td>
                <td>{vehicle.company.name}</td>
                <td>{vehicle.totalCapacity}</td>
                <td>{vehicleStatusText[vehicle.status]}</td>
                <td>
                  <span>
                    <button type="button">
                      <EditIcon />
                    </button>

                    <button
                      type="button"
                      onClick={(event) => {
                        event.stopPropagation()
                        event.nativeEvent.stopImmediatePropagation()
                        deleteVehicle(vehicle.id)
                      }}
                    >
                      <DeleteIcon />
                    </button>
                  </span>
                </td>
              </tr>
            ))}
        </tbody>
      </table>
      {(!data || data.length === 0) && <p>(vazio)</p>}
    </PageLayout>
  )
}
