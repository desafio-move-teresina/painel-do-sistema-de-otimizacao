import { useRouter } from 'next/router'
import { toast } from 'react-toastify'

import { vehiclesRepository } from 'repositories'
import { PageLayout } from 'components/PageLayout'
import { IVehicleCreateDTO } from 'repositories/IVehiclesRepository'
import { VehicleForm } from 'components/VehicleForm'

export default function CreateVehicle() {
  const router = useRouter()

  const onSubmit = async (values: IVehicleCreateDTO): Promise<void> => {
    try {
      await vehiclesRepository.create(values)
      toast.success('Veículo cadastrado')
      router.push('/vehicles')
    } catch (error) {
      toast.error('Erro ao cadastrar')
    }
  }

  return (
    <PageLayout headTitle="Cadastrar Veículos | Optime" title="Veículos">
      <VehicleForm title="Cadastrar Veículo" onSubmit={onSubmit} />
    </PageLayout>
  )
}
