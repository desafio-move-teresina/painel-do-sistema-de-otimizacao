import { AppProps } from 'next/app'
import { QueryClient, QueryClientProvider } from 'react-query'
import { ToastContainer, Slide } from 'react-toastify'
import Modal from 'react-modal'
import 'services/mirage'

import 'react-toastify/dist/ReactToastify.css'
import 'styles/global.scss'

const queryClient = new QueryClient()
Modal.setAppElement('#__next')

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <QueryClientProvider client={queryClient}>
      <Component {...pageProps} />
      <ToastContainer
        position="bottom-center"
        autoClose={3000}
        hideProgressBar={true}
        newestOnTop={true}
        transition={Slide}
        closeButton={false}
      />
    </QueryClientProvider>
  )
}

export default MyApp
