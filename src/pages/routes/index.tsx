import { useState, useEffect } from 'react'
import Head from 'next/head'
import { useQuery } from 'react-query'
import { GoogleMap, LoadScript } from '@react-google-maps/api'
import { toast } from 'react-toastify'

import {
  decisionVariablesRepository,
  mapsRepository,
  routesRepository,
} from 'repositories'
import { RoutesController } from 'components/RoutesController'
import { parseMapToAPI, parseRoutesFromAPI } from 'utils/mapParser'
import { MenuItem, BusStopMenuItem, RouteMenuItem } from 'utils/itemsController'
import { informQueryStatus } from 'utils/informQueryStatus'
import * as mapUtils from 'utils/mapUtils'

export type SelectedRoute = RouteMenuItem | 'all' | 'none'

export default function Routes() {
  /*
   * API calls
   */
  const query = useQuery(
    'routes',
    async () => {
      const responses = await Promise.all([
        decisionVariablesRepository.get(),
        routesRepository.list(),
        routesRepository.optimizations(),
      ])

      const [apiVariables, apiRoutes, apiOptimizations] = responses
      const defaultBusStopRadius: number =
        apiVariables?.defaultBusStopRadius || 600

      return { defaultBusStopRadius, apiRoutes, apiOptimizations }
    },
    { refetchOnWindowFocus: false, cacheTime: 0 }
  )

  /*
   * API Data state
   */
  const { status, data, refetch } = query

  useEffect(() => {
    informQueryStatus(status)

    if (data) {
      BusStopMenuItem.radius = data.defaultBusStopRadius
      return
    }
  }, [status, data])

  /*
   * Component state
   */

  /* Google map's map reference */
  const [map, setMap] = useState<google.maps.Map | null>(null)

  /* The menu item that is in editing mode. If none it's null */
  const [editableMenuItem, setEditableMenuItem] = useState<MenuItem | null>(
    null
  )

  /* The selected route. If none it's null */
  const [selectedRoute, setSelectedRoute] = useState<SelectedRoute>('none')

  /*
   * The selected route id. If none it's null. It's used to restore the selected
   * route after the route reorder
   */
  const [selectedRouteId, setSelectedRouteId] = useState<string | null>(null)

  /* Optimizations */
  const optimizations = data?.apiOptimizations ? data.apiOptimizations : []

  /* Bus stops menu items */
  const [busStopMenuItems, setBusStopMenuItems] = useState<BusStopMenuItem[]>(
    []
  )

  /* Route menu items */
  const [routeMenuItems, setRouteMenuItems] = useState<RouteMenuItem[]>([])

  /* Bus stop places modal state */
  const [isPlacesModalOpen, setIsPlacesModalOpen] = useState(false)

  /* Bus stop edition modal state */
  const [isEditBusStopModalOpen, setIsEditBusStopModalOpen] = useState(false)

  /*
   * Parsing API data
   */
  useEffect(() => {
    if (map && data?.apiRoutes) {
      const { apiRoutes } = data

      const { routeMenuItems, busStopMenuItems } = parseRoutesFromAPI(
        map,
        apiRoutes
      )

      setBusStopMenuItems(busStopMenuItems)
      setRouteMenuItems(routeMenuItems)

      // Resetting type of all bus stops menu items
      busStopMenuItems.forEach((busStopItem) => {
        busStopItem.type = 'hidden'
      })
    }
  }, [map, data])

  useEffect(() => {
    if (selectedRouteId && routeMenuItems.length > 0) {
      // Find the previously selected route by its id
      const routeItem = routeMenuItems.find(
        (routeItem) => routeItem.id === selectedRouteId
      )

      if (!routeItem) return

      // Setting it as the selected route
      routeItem.editable = true
      routeItem.setBusStopTypes()

      setSelectedRoute(routeItem)
    }
  }, [routeMenuItems, selectedRouteId])

  /*
   * Component state functions
   */

  /**
   * Set editable to false on polygons and bus stops
   * @param exceptMenuItem this menu item will not be affected
   */
  const endEdition = (exceptMenuItem: MenuItem | null = null) => {
    busStopMenuItems.forEach((busStopItem) => {
      if (busStopItem === exceptMenuItem) return
      busStopItem.editable = false
    })
  }

  /** Toggles menu item editable property */
  const toggleMenuItemEditable = (menuItem: MenuItem) => {
    const editable = !menuItem.editable

    // Clear all editions
    if (!editable) {
      endEdition()
      setEditableMenuItem(null)
      return
    }

    // End all other ongoing editions
    endEdition(menuItem)

    // Set editable
    setEditableMenuItem(menuItem)
    menuItem.editable = true
  }

  /** Add click listener to all bus stop menu items */
  useEffect(() => {
    busStopMenuItems.forEach((busStopItem) =>
      busStopItem.addClickListener(toggleMenuItemEditable)
    )
    // eslint-disable-next-line
  }, [busStopMenuItems])

  /** Updates the bus stops of the routes when a bus stop is selected, created or removed */
  const updateRoutesBusStops = (busStopMenuItems: BusStopMenuItem[]) => {
    const apiRoutes = data?.apiRoutes ? data.apiRoutes : []

    routeMenuItems.forEach((routeItem) => {
      const apiRoute = apiRoutes.find(
        (apiRoute) => apiRoute.id === routeItem.id
      )
      routeItem.updateBusStops(apiRoute?.busStops || [], busStopMenuItems)
    })
  }

  /** Changes the selected route in the page  */
  const changeSelectedRoute = (route: SelectedRoute) => {
    // Resetting type of all bus stops menu items
    busStopMenuItems.forEach((busStopItem) => {
      busStopItem.type = 'hidden'
    })

    // Resetting editable of all route menu items
    routeMenuItems.forEach((routeItem) => {
      routeItem.editable = false
    })

    updateRoutesBusStops(busStopMenuItems)

    if (route === 'all') {
      // Set bus stop visible
      busStopMenuItems.forEach((busStopItem) => {
        busStopItem.type = 'normal'
      })

      // Assign colors in order. You can use mapUtils.randomColor()
      // instead
      routeMenuItems.forEach((routeItem, index) => {
        routeItem.editable = true
        const colorIndex = index % mapUtils.colors.length
        routeItem.setColor(mapUtils.colors[colorIndex])
      })
      setSelectedRouteId('all')
    } else if (route instanceof RouteMenuItem) {
      route.editable = true
      route.setBusStopTypes()
      route.setColor()
      setSelectedRouteId(route.id)
    } else {
      setSelectedRouteId('none')
    }
    setSelectedRoute(route)
  }

  // FIXME: it's only recreating the array
  const updateBusStopMenuItem = () => {
    setBusStopMenuItems([...busStopMenuItems])
  }

  const deleteBusStopMenuItem = (deletedBusStopItem: BusStopMenuItem) => {
    const newItems = busStopMenuItems.filter(
      (busStopItem) => busStopItem !== deletedBusStopItem
    )
    deletedBusStopItem.dispose()
    updateRoutesBusStops(newItems)

    setBusStopMenuItems(newItems)
  }

  /** Clear most of the page state. Used before reload the map features */
  const clearAll = () => {
    busStopMenuItems.forEach((busStopItem) => busStopItem.dispose())
    routeMenuItems.forEach((routeItem) => routeItem.dispose())

    BusStopMenuItem.itemsCount = 1
    setBusStopMenuItems([])
    setRouteMenuItems([])

    setEditableMenuItem(null)
    setSelectedRoute('none')
    setIsPlacesModalOpen(false)
    setIsEditBusStopModalOpen(false)
  }

  /**
   * On google map load complete callback
   */
  const onMapLoad = (mapRef: google.maps.Map) => {
    setMap(mapRef)

    // The SVG path is 14 x 16px, anchoring half of this to center the marker
    const point = new google.maps.Point(7, 8)
    mapUtils.svgMarker.anchor = point
    mapUtils.svgMarkerInactive.anchor = point
    mapUtils.svgMarkerRoute.anchor = point
  }

  /**
   * On save button click callback
   */
  const onSaveClick = async () => {
    endEdition()
    setEditableMenuItem(null)
    const apiMap = parseMapToAPI([], busStopMenuItems)

    try {
      // The routes page don't have the areaPoints (polygon) anymore.
      // The API still require it. The API should probably be changed
      // to accept saving the map without area and areaPoints.
      // Also the API is not returning the property area of the map.
      const { area, areaPoints } = await mapsRepository.get()
      apiMap.area = area || 0
      apiMap.areaPoints = areaPoints

      await mapsRepository.create(apiMap)
      toast.success('Mapa salvo')
    } catch (error) {
      toast.error('Erro ao salvar')
    }
  }

  /**
   * On route reprocess button click callback
   */
  const onRouteReprocessClick = async (routeId?: string) => {
    if (!routeId) return

    setSelectedRouteId(routeId)

    try {
      await onSaveClick()
      await routesRepository.reorder(routeId)

      clearAll()
      await refetch()
      toast.success('Rota reprocessada')
    } catch (error) {
      toast.error('Erro ao reprocessar')
    }
  }

  /*
   * Component structure
   */
  return (
    <>
      <Head>
        <title>Rotas | Optime</title>
      </Head>

      <RoutesController
        onSaveClick={onSaveClick}
        onRouteReprocessClick={onRouteReprocessClick}
        editableMenuItem={editableMenuItem}
        busStopMenuItems={busStopMenuItems}
        routeMenuItems={routeMenuItems}
        selectedRoute={selectedRoute}
        changeSelectedRoute={changeSelectedRoute}
        optimizations={optimizations}
        toggleMenuItemEditable={toggleMenuItemEditable}
        updateBusStopMenuItem={updateBusStopMenuItem}
        deleteBusStopMenuItem={deleteBusStopMenuItem}
        isPlacesModalOpen={isPlacesModalOpen}
        setIsPlacesModalOpen={setIsPlacesModalOpen}
        isEditBusStopModalOpen={isEditBusStopModalOpen}
        setIsEditBusStopModalOpen={setIsEditBusStopModalOpen}
      />

      <LoadScript
        googleMapsApiKey={process.env.NEXT_PUBLIC_GOOGLE_MAPS_API_KEY || ''}
        libraries={mapUtils.mapLibraries}
      >
        <GoogleMap
          id="map-editor"
          onLoad={onMapLoad}
          options={mapUtils.mapOptions}
          mapContainerStyle={mapUtils.mapContainerStyle}
          center={mapUtils.mapCenter}
          zoom={mapUtils.mapZoom}
        />
      </LoadScript>
    </>
  )
}
