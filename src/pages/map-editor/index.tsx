import { useState, useEffect } from 'react'
import Head from 'next/head'
import { useQuery } from 'react-query'
import { GoogleMap, LoadScript, DrawingManager } from '@react-google-maps/api'
import { toast } from 'react-toastify'

import { decisionVariablesRepository, mapsRepository } from 'repositories'
import { MapEditorController } from 'components/MapEditorController'
import { DrawingTool } from 'components/MapEditorController/MenuHeader'
import {
  parseBusStopFromAPI,
  parseMapFromAPI,
  parseMapToAPI,
} from 'utils/mapParser'
import * as mapUtils from 'utils/mapUtils'

import {
  MenuItem,
  PolygonMenuItem,
  BusStopMenuItem,
} from 'utils/itemsController'
import { informQueryStatus } from 'utils/informQueryStatus'

export default function MapEditor() {
  /*
   * API calls
   */
  const query = useQuery(
    'map-editor',
    async () => {
      const responses = await Promise.all([
        decisionVariablesRepository.get(),
        mapsRepository.get(),
      ])

      const [apiVariables, apiMap] = responses
      const defaultBusStopRadius: number =
        apiVariables?.defaultBusStopRadius || 600

      return { defaultBusStopRadius, apiMap }
    },
    { refetchOnWindowFocus: false, cacheTime: 0 }
  )

  /*
   * API Data state
   */
  const { status, data } = query

  useEffect(() => {
    informQueryStatus(status)

    if (data) {
      BusStopMenuItem.radius = data.defaultBusStopRadius
      return
    }
  }, [status, data])

  /*
   * Component state
   */

  /* Google map's map reference */
  const [map, setMap] = useState<google.maps.Map | null>(null)

  /* Current selected drawing tool */
  const [drawingTool, setDrawingTool] = useState<DrawingTool>('PAN')

  /* The menu item that is in editing mode. If none it's null */
  const [editableMenuItem, setEditableMenuItem] = useState<MenuItem | null>(
    null
  )

  /* Polygons menu items */
  const [polygonMenuItems, setPolygonMenuItems] = useState<PolygonMenuItem[]>(
    []
  )

  /* Bus stops menu items */
  const [busStopMenuItems, setBusStopMenuItems] = useState<BusStopMenuItem[]>(
    []
  )

  /* Bus stop places modal state */
  const [isPlacesModalOpen, setIsPlacesModalOpen] = useState(false)

  /* Bus stop edition modal state */
  const [isEditBusStopModalOpen, setIsEditBusStopModalOpen] = useState(false)

  /*
   * Parsing API data
   */
  useEffect(() => {
    if (map && data?.apiMap) {
      const { apiMap } = data
      const { polygonMenuItems, busStopMenuItems } = parseMapFromAPI(
        map,
        apiMap
      )
      setPolygonMenuItems(polygonMenuItems)
      setBusStopMenuItems(busStopMenuItems)
    }
  }, [map, data])

  /**
   * Only bus stops that are inside the polygon are shown.
   * When the polygon changes this function fetches and updates all bus
   * stops from the map.
   */
  useEffect(() => {
    const onPolygonChange = async () => {
      const polygonItem = polygonMenuItems[0]

      if (!polygonItem || !map) return

      try {
        polygonItem.editable = false

        // Parse map for save
        const apiMap = parseMapToAPI(polygonMenuItems, busStopMenuItems)

        // Save map. Required because a bus stop could be created
        // updated or removed
        await mapsRepository.create(apiMap)

        // Fetch bus stops from the polygon area
        const apiBusStops = await mapsRepository.areaBusStops(
          polygonItem.points
        )

        BusStopMenuItem.itemsCount = 1

        // Parse bus stops API data
        const busStopItems = apiBusStops.map((apiBusStop) =>
          parseBusStopFromAPI(map, apiBusStop)
        )

        // Dispose current bus stops
        busStopMenuItems.forEach((busStopItem) => {
          busStopItem.dispose()
        })

        // Set new bus stops
        setBusStopMenuItems(busStopItems)

        polygonItem.editable = true
      } catch (error) {
        polygonItem.editable = true
        toast.error('Erro ao carregar a área')
      }
    }

    // Every time busStopMenuItems array change the event listener must be
    // update or it will reference the old array
    const polygonItem = polygonMenuItems[0]
    polygonItem?.addEventListener(onPolygonChange)
  }, [map, busStopMenuItems, polygonMenuItems])

  /*
   * Component state functions
   */

  /**
   * Set editable to false on polygons and bus stops
   * @param exceptMenuItem this menu item will not be affected
   */
  const endEdition = (exceptMenuItem: MenuItem | null = null) => {
    polygonMenuItems.forEach((polygonItem) => {
      if (polygonItem === exceptMenuItem) return
      polygonItem.editable = false
    })

    busStopMenuItems.forEach((busStopItem) => {
      if (busStopItem === exceptMenuItem) return
      busStopItem.editable = false
    })
  }

  /** Toggles menu item editable property */
  const toggleMenuItemEditable = (menuItem: MenuItem) => {
    const editable = !menuItem.editable

    // Clear all editions
    if (!editable) {
      endEdition()
      setEditableMenuItem(null)
      return
    }

    // Google Maps need to be in the pan mode to drag and edit items
    setDrawingTool('PAN')

    // End all other ongoing editions
    endEdition(menuItem)

    // Set editable
    setEditableMenuItem(menuItem)
    menuItem.editable = true
  }

  /** Add click listener to all bus stop menu items */
  useEffect(() => {
    busStopMenuItems.forEach((busStopItem) =>
      busStopItem.addClickListener(toggleMenuItemEditable)
    )
    // eslint-disable-next-line
  }, [busStopMenuItems])

  // FIXME: it's only recreating the array
  const updateBusStopMenuItem = () => {
    setBusStopMenuItems([...busStopMenuItems])
  }

  const deletePolygonMenuItem = (deletedPolygonItem: PolygonMenuItem) => {
    const newItems = polygonMenuItems.filter(
      (polygonItem) => polygonItem !== deletedPolygonItem
    )
    deletedPolygonItem.dispose()
    setPolygonMenuItems(newItems)
  }

  const deleteBusStopMenuItem = (deletedBusStopItem: BusStopMenuItem) => {
    const newItems = busStopMenuItems.filter(
      (busStopItem) => busStopItem !== deletedBusStopItem
    )
    deletedBusStopItem.dispose()
    setBusStopMenuItems(newItems)
  }

  /** Returns drawing mode based on current selected drawing tool */
  const getDrawingMode = (): google.maps.drawing.OverlayType | null => {
    switch (drawingTool) {
      case 'PAN':
        return null
      case 'POLYGON':
        return google.maps.drawing.OverlayType.POLYGON
      case 'BUS_STOP':
        return google.maps.drawing.OverlayType.MARKER
    }
  }

  /**
   * On google map load complete callback
   */
  const onMapLoad = (mapRef: google.maps.Map) => {
    setMap(mapRef)

    // The SVG path is 14 x 16px, anchoring half of this to center the marker
    const point = new google.maps.Point(7, 8)
    mapUtils.svgMarker.anchor = point
    mapUtils.svgMarkerInactive.anchor = point
    mapUtils.svgMarkerRoute.anchor = point
  }

  /**
   * On drawing tool buttons click callback
   */
  const onDrawingToolClick = (tool: DrawingTool) => {
    if (tool === 'POLYGON' && polygonMenuItems.length > 0) {
      toast.info('O mapa pode ter apenas uma área', { toastId: 'one-polygon' })
      return
    }
    endEdition()
    setEditableMenuItem(null)
    setDrawingTool(tool)
  }

  /**
   * On save button click callback
   */
  const onSaveClick = async () => {
    endEdition()
    setEditableMenuItem(null)
    const apiMap = parseMapToAPI(polygonMenuItems, busStopMenuItems)

    try {
      await mapsRepository.create(apiMap)
      toast.success('Mapa salvo')
    } catch (error) {
      toast.error('Erro ao salvar')
    }
  }

  /**
   * On process button click callback
   */
  const onMapProcess = async (routesCount: number) => {
    if (routesCount < 1) {
      toast.info('Número invalido')
      return
    }

    endEdition()
    setEditableMenuItem(null)
    const apiMap = parseMapToAPI(polygonMenuItems, busStopMenuItems)

    try {
      await mapsRepository.create(apiMap)
      await mapsRepository.process(routesCount)
      toast.info('Processamento solicitado')
    } catch (error) {
      toast.error('Erro na solicitação')
    }
  }

  /*
   * On drawing complete callbacks
   */

  /** New polygons creates a PolygonMenuItem. They are stored in the polygonMenuItems array */
  const onPolygonComplete = (polygon: google.maps.Polygon) => {
    const newItems = [...polygonMenuItems]
    newItems.push(new PolygonMenuItem(polygon))
    setPolygonMenuItems(newItems)
    setDrawingTool('PAN')
  }

  /** New makers creates a BusStopMenuItem. They are stored in busStopMenuItems array */
  const onMarkerComplete = (marker: google.maps.Marker) => {
    const newItems = [...busStopMenuItems]
    newItems.push(new BusStopMenuItem(marker))
    setBusStopMenuItems(newItems)
  }

  /*
   * Component structure
   */
  return (
    <>
      <Head>
        <title>Mapa | Optime</title>
      </Head>

      <MapEditorController
        drawingTool={drawingTool}
        onDrawingToolClick={onDrawingToolClick}
        onSaveClick={onSaveClick}
        onMapProcess={onMapProcess}
        editableMenuItem={editableMenuItem}
        polygonMenuItems={polygonMenuItems}
        busStopMenuItems={busStopMenuItems}
        toggleMenuItemEditable={toggleMenuItemEditable}
        updateBusStopMenuItem={updateBusStopMenuItem}
        deletePolygonMenuItem={deletePolygonMenuItem}
        deleteBusStopMenuItem={deleteBusStopMenuItem}
        isPlacesModalOpen={isPlacesModalOpen}
        setIsPlacesModalOpen={setIsPlacesModalOpen}
        isEditBusStopModalOpen={isEditBusStopModalOpen}
        setIsEditBusStopModalOpen={setIsEditBusStopModalOpen}
      />

      <LoadScript
        googleMapsApiKey={process.env.NEXT_PUBLIC_GOOGLE_MAPS_API_KEY || ''}
        libraries={mapUtils.mapLibraries}
      >
        <GoogleMap
          id="map-editor"
          onLoad={onMapLoad}
          options={mapUtils.mapOptions}
          mapContainerStyle={mapUtils.mapContainerStyle}
          center={mapUtils.mapCenter}
          zoom={mapUtils.mapZoom}
        >
          <DrawingManager
            options={mapUtils.drawingManagerOptions}
            drawingMode={getDrawingMode()}
            onPolygonComplete={onPolygonComplete}
            onMarkerComplete={onMarkerComplete}
          />
        </GoogleMap>
      </LoadScript>
    </>
  )
}
