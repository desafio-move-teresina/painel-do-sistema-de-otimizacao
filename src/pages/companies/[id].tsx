import { useEffect } from 'react'
import { useRouter } from 'next/router'
import { useQuery } from 'react-query'
import { toast } from 'react-toastify'

import { companiesRepository } from 'repositories'
import { PageLayout } from 'components/PageLayout'
import { ICompanyCreateDTO } from 'repositories/ICompaniesRepository'
import { CompanyForm } from 'components/CompanyForm'
import { informQueryStatus } from 'utils/informQueryStatus'

export default function EditCompany() {
  const router = useRouter()
  const { id: companyId } = router.query

  const query = useQuery(
    ['company', companyId],
    () => companiesRepository.get(companyId as string),
    { enabled: !!companyId && typeof companyId === 'string' }
  )

  const { status, data } = query

  useEffect(() => {
    informQueryStatus(status)
  }, [status])

  const onSubmit = async (values: ICompanyCreateDTO): Promise<void> => {
    try {
      if (typeof companyId !== 'string') {
        throw new Error('No company id')
      }
      const company = { id: companyId, ...values }
      await companiesRepository.edit(companyId, company)
      toast.success('Empresa atualizada')
      router.push('/companies')
    } catch (error) {
      toast.error('Erro ao atualizar')
    }
  }

  return (
    <PageLayout headTitle="Editar Empresa | Optime" title="Empresas">
      <CompanyForm title="Editar Empresa" company={data} onSubmit={onSubmit} />
    </PageLayout>
  )
}
