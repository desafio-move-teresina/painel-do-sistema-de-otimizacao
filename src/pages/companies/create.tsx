import { useRouter } from 'next/router'
import { toast } from 'react-toastify'

import { companiesRepository } from 'repositories'
import { PageLayout } from 'components/PageLayout'
import { ICompanyCreateDTO } from 'repositories/ICompaniesRepository'
import { CompanyForm } from 'components/CompanyForm'

export default function CreateCompany() {
  const router = useRouter()

  const onSubmit = async (values: ICompanyCreateDTO): Promise<void> => {
    try {
      await companiesRepository.create(values)
      toast.success('Empresa cadastrada')
      router.push('/companies')
    } catch (error) {
      toast.error('Erro ao cadastrar')
    }
  }

  return (
    <PageLayout headTitle="Cadastrar Empresa | Optime" title="Empresas">
      <CompanyForm title="Cadastrar Empresa" onSubmit={onSubmit} />
    </PageLayout>
  )
}
