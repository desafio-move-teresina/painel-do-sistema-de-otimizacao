import { useEffect } from 'react'
import { useRouter } from 'next/router'
import { useQuery, useQueryClient } from 'react-query'
import { IoMdAddCircle as AddIcon } from 'react-icons/io'
import { FaBus as BusIcon } from 'react-icons/fa'
import { MdModeEdit as EditIcon } from 'react-icons/md'
import { MdDelete as DeleteIcon } from 'react-icons/md'
import { toast } from 'react-toastify'

import { companiesRepository } from 'repositories'
import { PageLayout } from 'components/PageLayout'
import { informQueryStatus } from 'utils/informQueryStatus'
import styles from './styles.module.scss'

export default function Companies() {
  const query = useQuery('companies', companiesRepository.list)

  const queryClient = useQueryClient()

  const { status, data } = query

  useEffect(() => {
    informQueryStatus(status)
  }, [status])

  const router = useRouter()

  const deleteCompany = async (companyId: string) => {
    try {
      if (!data) {
        return
      }

      await companiesRepository.delete(companyId)
      const newCompanies = data.filter((company) => company.id !== companyId)
      queryClient.setQueryData('companies', newCompanies)
    } catch (error) {
      toast.error('Erro ao excluir')
    }
  }

  return (
    <PageLayout
      headTitle="Empresas | Optime"
      title="Empresas"
      className={styles.container}
    >
      <header>
        <button type="button" onClick={() => router.push('/companies/create')}>
          <AddIcon />
        </button>
      </header>

      <table>
        <thead>
          <tr>
            <th>Empresa</th>
            <th>Consórcio</th>
            <th>Veículos</th>
            <th>Contato</th>
          </tr>
        </thead>

        <tbody>
          {data &&
            data.map((company) => (
              <tr
                key={company.id}
                onClick={() => router.push(`/companies/${company.id}`)}
              >
                <td>{company.name}</td>
                <td>{company.consortium}</td>
                <td>0</td>
                <td>{company.phone}</td>
                <td>
                  <span>
                    <button
                      type="button"
                      onClick={(event) => {
                        event.stopPropagation()
                        event.nativeEvent.stopImmediatePropagation()
                        router.push(`/vehicles?companyId=${company.id}`)
                      }}
                    >
                      <BusIcon />
                    </button>

                    <button type="button">
                      <EditIcon />
                    </button>

                    <button
                      type="button"
                      onClick={(event) => {
                        event.stopPropagation()
                        event.nativeEvent.stopImmediatePropagation()
                        deleteCompany(company.id)
                      }}
                    >
                      <DeleteIcon />
                    </button>
                  </span>
                </td>
              </tr>
            ))}
        </tbody>
      </table>
      {(!data || data.length === 0) && <p>(vazio)</p>}
    </PageLayout>
  )
}
