import * as mapUtils from 'utils/mapUtils'

/**
 * Map editor left menu item
 */
export interface MenuItem {
  name: string
  editable: boolean
  onMouseEnter(): void
  onMouseLeave(): void
  dispose(): void
}

export interface Point {
  latitude: number
  longitude: number
}

/**
 * Map editor polygon menu item
 * Holds a google maps polygon
 */
export class PolygonMenuItem implements MenuItem {
  static itemsCount = 1

  name: string
  private _polygon: google.maps.Polygon

  // Event listeners removers
  private _elr1: google.maps.MapsEventListener | null = null
  private _elr2: google.maps.MapsEventListener | null = null
  private _elr3: google.maps.MapsEventListener | null = null
  private _elr4: google.maps.MapsEventListener | null = null

  constructor(polygon: google.maps.Polygon) {
    this.name = `Área ${PolygonMenuItem.itemsCount++}`
    this._polygon = polygon
    this._polygon.setOptions(mapUtils.polygonOptions)
  }

  get editable() {
    return this._polygon.getEditable()
  }

  set editable(editable: boolean) {
    this._polygon.setEditable(editable)
    this.updatePolygonOptions()
  }

  get area() {
    const path = this._polygon.getPath()
    const area = google.maps.geometry.spherical.computeArea(path)
    return area
  }

  onMouseEnter() {
    this._polygon.setOptions(mapUtils.polygonEditingOptions)
  }

  onMouseLeave() {
    this.updatePolygonOptions()
  }

  dispose() {
    this._polygon.setMap(null)
  }

  get points(): Point[] {
    const mvcArray = this._polygon.getPath()
    const points = mvcArray.getArray().map((latLng) => {
      return {
        latitude: latLng.lat(),
        longitude: latLng.lng(),
      }
    })
    return points
  }

  /** Adds the callback that will be called when the polygon changes */
  public addEventListener(callback: () => void) {
    // Removing existing event listeners
    this._elr1?.remove()
    this._elr2?.remove()
    this._elr3?.remove()
    this._elr4?.remove()

    const mvcArray: google.maps.MVCArray<google.maps.LatLng> =
      this._polygon.getPath()

    // Add updated listeners
    this._elr1 = this._polygon.addListener('dragend', callback)
    this._elr2 = mvcArray.addListener('insert_at', callback)
    this._elr3 = mvcArray.addListener('remove_at', callback)
    this._elr4 = mvcArray.addListener('set_at', callback)
  }

  private updatePolygonOptions() {
    const options = this.editable
      ? mapUtils.polygonEditingOptions
      : mapUtils.polygonOptions
    this._polygon.setOptions(options)
  }
}

type BusStopType = 'normal' | 'route' | 'hidden'

/**
 * Map editor bus stop menu item
 * Holds a google maps marker and circle
 */
export class BusStopMenuItem implements MenuItem {
  static itemsCount = 1

  static radius = 600
  static direction = 0
  static isActive = true
  static passengersIn = 15
  static passengersOut = 15

  id?: string
  name: string
  direction = BusStopMenuItem.direction
  passengersIn = BusStopMenuItem.passengersIn
  passengersOut = BusStopMenuItem.passengersOut

  private _isActive = true
  private _marker: google.maps.Marker
  private _circle: google.maps.Circle
  private _infoWindow: google.maps.InfoWindow

  private _type: BusStopType = 'normal'
  private _circleVisible = false

  /** Event listeners remover */
  private _elr: google.maps.MapsEventListener | null = null

  /** Creates a bus stop composed by a marker and a circle */
  constructor(marker: google.maps.Marker, id?: string) {
    this.name = `Ponto ${BusStopMenuItem.itemsCount++}`
    this.id = id

    this._marker = marker
    this._marker.setOptions(mapUtils.markerOptions)

    this._circle = new google.maps.Circle(mapUtils.circleOptions)
    this._infoWindow = new google.maps.InfoWindow({ content: this.name })

    this.radius = BusStopMenuItem.radius
    this.isActive = BusStopMenuItem.isActive

    const position = this._marker.getPosition()
    if (position instanceof google.maps.LatLng) {
      this._circle.setCenter(position)
    } else {
      throw new Error('Could not set the circle center')
    }

    const map = this._marker.getMap()
    if (map instanceof google.maps.Map) {
      this._circle.setMap(map)
    } else {
      throw new Error('Could not set the circle map')
    }

    // The circle position is updated when the marker is dragged
    this._marker.addListener('drag', (event: google.maps.MapMouseEvent) => {
      this._circle.setCenter(event.latLng)
    })

    // Display tooltip (infoWindow) when mouse enter
    this._marker.addListener('mouseover', () => {
      this._infoWindow.open({ anchor: this._marker, map, shouldFocus: false })
    })

    // Close tooltip (infoWindow) when mouse leave
    this._marker.addListener('mouseout', () => {
      this._infoWindow.close()
    })
  }

  get radius() {
    return this._circle.getRadius()
  }

  set radius(radius: number) {
    this._circle.setRadius(radius)
  }

  get isActive() {
    return this._isActive
  }

  set isActive(isActive: boolean) {
    this._isActive = isActive
    this.updateCircleOptions()
    this.updateMarkerOptions()
  }

  get type() {
    return this._type
  }

  set type(type: BusStopType) {
    this._type = type
    this.updateCircleOptions()
    this.updateMarkerOptions()
  }

  get circleVisible() {
    return this._circleVisible
  }

  set circleVisible(circleVisible: boolean) {
    this._circleVisible = circleVisible
    this.updateCircleOptions()
    this.updateMarkerOptions()
  }

  get editable() {
    // FIXME: getDraggable() returning undefined
    // For some reason this._marker.getDraggable() is sometimes
    // returning undefined. This should not be possible. It's
    // probably a bug here or on Google Maps API
    return !!this._marker.getDraggable()
  }

  set editable(editable: boolean) {
    this._marker.setDraggable(editable)
    this.updateCircleOptions()
  }

  get map() {
    return this._marker.getMap()
  }

  onMouseEnter() {
    // Forcing editable style on hover (on mouse enter)
    this.updateCircleOptions(true)
  }

  onMouseLeave() {
    this.updateCircleOptions()
  }

  /** Add click listener. The callback will be called when the marker is clicked */
  addClickListener(callback: (busStopItem: BusStopMenuItem) => void) {
    // Removing existing event listeners
    this._elr?.remove()
    // Add the new event listener
    this._elr = this._marker.addListener('click', () => callback(this))
  }

  dispose() {
    this._marker.setMap(null)
    this._circle.setMap(null)
  }

  get position(): Point {
    const markerPosition = this._marker.getPosition()

    if (!markerPosition) {
      throw new Error('The marker do not have a position')
    }

    const position = {
      latitude: markerPosition?.lat(),
      longitude: markerPosition?.lng(),
    }

    return position
  }

  // TODO: refactor circle options
  private updateCircleOptions(editable = this.editable) {
    // If it's hidden
    const isVisible = this.type !== 'hidden'
    this._circle.setVisible(isVisible)
    this._marker.setVisible(isVisible)

    if (!isVisible) return

    // If it's route circle and it's active
    if (this.type === 'route' && this.isActive) {
      if (editable) {
        this._circle.setOptions(mapUtils.circleRouteEditingOptions)
        this._circle.setVisible(true)
      } else {
        this._circle.setOptions(mapUtils.circleRouteOptions)
        this._circle.setVisible(this.circleVisible)
      }
      return
    }

    // Other
    if (editable && this.isActive) {
      this._circle.setOptions(mapUtils.circleEditingOptions)
    } else if (editable && !this.isActive) {
      this._circle.setOptions(mapUtils.circleInactiveEditingOptions)
    } else if (!editable && this.isActive) {
      this._circle.setOptions(mapUtils.circleOptions)
    } else if (!editable && !this.isActive) {
      this._circle.setOptions(mapUtils.circleInactiveOptions)
    }

    // Circle visibility
    if (editable) {
      this._circle.setVisible(true)
    } else {
      this._circle.setVisible(this.circleVisible)
    }
  }

  private updateMarkerOptions() {
    if (this.isActive && this.type === 'normal') {
      this._marker.setOptions(mapUtils.markerOptions)
    } else if (this.isActive && this.type === 'route') {
      this._marker.setOptions(mapUtils.markerRouteOptions)
    } else {
      this._marker.setOptions(mapUtils.markerInactiveOptions)
    }
  }
}

/** From all bus stop items on the map, find the ones that belong to a particular route */
const busStopIntersection = (
  apiItems: { id?: string }[],
  menuItems: BusStopMenuItem[]
) => {
  const apiItemsIds = apiItems.map((apiItem) => apiItem.id)
  const intersection = menuItems.filter((menuItem) =>
    apiItemsIds.includes(menuItem.id)
  )
  return intersection
}

/**
 * Routes menu item
 * Holds a google maps polyline
 */
export class RouteMenuItem implements MenuItem {
  name: string
  id: string
  optimizationId: string

  /** The array of bus stops that belongs to this route */
  busStopMenuItems: BusStopMenuItem[] = []

  private _polyline: google.maps.Polyline
  private _polylineOptions: google.maps.PolylineOptions
  private _editable = false

  constructor(
    polyline: string,
    map: google.maps.Map,
    name: string,
    id: string,
    optimizationId: string
  ) {
    this.name = name
    this.id = id
    this.optimizationId = optimizationId

    const path = google.maps.geometry.encoding.decodePath(polyline)

    this._polylineOptions = { ...mapUtils.polylineOptions }
    this._polyline = new google.maps.Polyline(this._polylineOptions)
    this._polyline.setPath(path)
    this._polyline.setMap(map)
  }

  get editable() {
    return this._editable
  }

  set editable(editable: boolean) {
    this._editable = editable
    this.updatePolylineOptions()
  }

  setColor(color = mapUtils.colors[0], visible = true) {
    this._polylineOptions.strokeColor = color
    this._polyline.setOptions(this._polylineOptions)
    this._polyline.setVisible(visible)
  }

  onMouseEnter() {
    this._polyline.setVisible(true)
  }

  onMouseLeave() {
    this.updatePolylineOptions()
  }

  dispose() {
    this._polyline.setMap(null)
  }

  updateBusStops(apiItems: { id?: string }[], menuItems: BusStopMenuItem[]) {
    this.busStopMenuItems = busStopIntersection(apiItems, menuItems)
  }

  setBusStopTypes() {
    this.busStopMenuItems.forEach((busStopItem) => {
      busStopItem.type = 'route'
    })
  }

  updatePolylineOptions() {
    if (this.editable) {
      this._polyline.setVisible(true)
    } else {
      this._polyline.setVisible(false)
    }
  }
}
