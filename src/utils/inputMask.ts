export const nonDigitsRgx = /[^0-9]/g
export const nonCnpjCharsRgx = /[^0-9\.\/-]/g
export const nonPhoneCharsRgx = /[^0-9\(\)-\s]/g

/**
 * Mask in the format: 99.999.999/9999-99
 */
export function cnpjMask(cnpj: string) {
  const digits = cnpj.replaceAll(nonDigitsRgx, '')
  const len = digits.length

  if (len === 3) return `${digits.substr(0, 2)}.${digits.substr(2, 1)}`

  if (len === 6)
    return `${digits.substr(0, 2)}.${digits.substr(2, 3)}.${digits.substr(
      5,
      1
    )}`

  if (len === 9)
    return `${digits.substr(0, 2)}.${digits.substr(2, 3)}.${digits.substr(
      5,
      3
    )}/${digits.substr(8, 1)}`

  if (len === 13)
    return `${digits.substr(0, 2)}.${digits.substr(2, 3)}.${digits.substr(
      5,
      3
    )}/${digits.substr(8, 4)}-${digits.substr(12, 1)}`

  if (len > 14 || cnpj.length > 18)
    return `${digits.substr(0, 2)}.${digits.substr(2, 3)}.${digits.substr(
      5,
      3
    )}/${digits.substr(8, 4)}-${digits.substr(12, 2)}`

  return cnpj.replaceAll(nonCnpjCharsRgx, '')
}

/**
 * Mask in the format: (99) 9999-9999
 */
export function phoneMask(phone: string) {
  const digits = phone.replaceAll(nonDigitsRgx, '')
  const len = digits.length

  if (len === 0) return ''

  if (len === 1) return `(${digits}`

  if (len === 3) return `(${digits.substr(0, 2)}) ${digits.substr(2, 1)}`

  if (len === 7)
    return `(${digits.substr(0, 2)}) ${digits.substr(2, 4)}-${digits.substr(
      6,
      1
    )}`

  if (len > 10 || phone.length > 14)
    return `(${digits.substr(0, 2)}) ${digits.substr(2, 4)}-${digits.substr(
      6,
      4
    )}`

  return phone.replaceAll(nonPhoneCharsRgx, '')
}
