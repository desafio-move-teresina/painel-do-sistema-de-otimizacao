import { QueryStatus } from 'react-query'
import { toast } from 'react-toastify'

export function informQueryStatus(status: QueryStatus) {
  switch (status) {
    case 'loading':
      toast.info('Carregando...', {
        toastId: 'query-status',
        autoClose: false,
      })
      return

    case 'error':
      toast.update('query-status', {
        type: toast.TYPE.ERROR,
        render: 'Tente mais tarde',
        autoClose: 3000,
      })
      return

    case 'success':
      toast.dismiss('query-status')
      return
  }
}
