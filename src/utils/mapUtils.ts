import { Libraries } from '@react-google-maps/api/dist/utils/make-load-script-url'

/*
 * Map
 */

export const mapLibraries: Libraries = ['geometry', 'drawing', 'places']

export const mapCenter = { lat: -5.09047, lng: -42.80419 }

export const mapZoom = 13

export const mapOptions: google.maps.MapOptions = {
  disableDefaultUI: true,
}

export const mapContainerStyle = {
  width: '100vw',
  height: '100vh',
}

export const mapContainerStyleReports = {
  width: '100%',
  height: '480px',
}

/*
 * SVG
 */

export const svgMarker: google.maps.Symbol = {
  path: 'M13.3438 4H13.125V2.5C13.125 1.1 10.4125 0 7 0C3.5875 0 0.875 1.1 0.875 2.5V4H0.65625C0.293945 4 0 4.33562 0 4.75V7.25C0 7.66406 0.293945 8 0.65625 8H0.875V13C0.875 13.5522 1.26684 14 1.75 14V15C1.75 15.5522 2.14184 16 2.625 16H3.5C3.98316 16 4.375 15.5522 4.375 15V14H9.625V15C9.625 15.5522 10.0168 16 10.5 16H11.375C11.8582 16 12.25 15.5522 12.25 15V14H12.425C12.8625 14 13.125 13.6 13.125 13.2V8H13.3438C13.7061 8 14 7.66406 14 7.25V4.75C14 4.33562 13.7061 4 13.3438 4ZM3.0625 12.5C2.57934 12.5 2.1875 12.0522 2.1875 11.5C2.1875 10.9478 2.57934 10.5 3.0625 10.5C3.54566 10.5 3.9375 10.9478 3.9375 11.5C3.9375 12.0522 3.54566 12.5 3.0625 12.5ZM3.5 9C3.01684 9 2.625 8.55219 2.625 8V4C2.625 3.44781 3.01684 3 3.5 3H10.5C10.9832 3 11.375 3.44781 11.375 4V8C11.375 8.55219 10.9832 9 10.5 9H3.5ZM10.9375 12.5C10.4543 12.5 10.0625 12.0522 10.0625 11.5C10.0625 10.9478 10.4543 10.5 10.9375 10.5C11.4207 10.5 11.8125 10.9478 11.8125 11.5C11.8125 12.0522 11.4207 12.5 10.9375 12.5Z',
  fillColor: 'red',
  fillOpacity: 0.6,
  strokeWeight: 0,
  // Setting a Point to anchor here throws
  // "ReferenceError: google is not defined"
  // The anchor will be set on the onMapLoad callback.
  anchor: undefined,
}

export const svgMarkerInactive: google.maps.Symbol = {
  ...svgMarker,
  fillColor: 'gray',
  fillOpacity: 0.8,
}

export const svgMarkerRoute: google.maps.Symbol = {
  ...svgMarker,
  fillColor: '#009148',
  fillOpacity: 0.8,
}

/*
 * Marker
 */

export const markerOptions: google.maps.MarkerOptions = {
  icon: svgMarker,
}

export const markerInactiveOptions: google.maps.MarkerOptions = {
  icon: svgMarkerInactive,
}

export const markerRouteOptions: google.maps.MarkerOptions = {
  icon: svgMarkerRoute,
}

/*
 * Polygon
 */

export const polygonOptions: google.maps.PolygonOptions = {
  strokeColor: 'blue',
  strokeOpacity: 0.3,
  strokeWeight: 2,
  fillColor: 'blue',
  fillOpacity: 0.03,
  zIndex: 1,
  geodesic: false,
}

export const polygonEditingOptions: google.maps.PolygonOptions = {
  ...polygonOptions,
  strokeOpacity: 0.9,
  fillOpacity: 0.1,
}

/*
 * Circle
 */

export const circleOptions: google.maps.CircleOptions = {
  strokeColor: 'red',
  strokeOpacity: 0.3,
  strokeWeight: 2,
  fillColor: 'red',
  fillOpacity: 0.03,
  zIndex: 1,
}

export const circleEditingOptions: google.maps.CircleOptions = {
  ...circleOptions,
  strokeOpacity: 0.9,
  fillOpacity: 0.1,
}

export const circleInactiveOptions: google.maps.CircleOptions = {
  strokeOpacity: 0,
  strokeWeight: 2,
  fillOpacity: 0,
  zIndex: 1,
}

export const circleInactiveEditingOptions: google.maps.CircleOptions = {
  ...circleInactiveOptions,
  strokeColor: '#808080',
  strokeOpacity: 0.5,
  fillColor: '#a0a0a0',
  fillOpacity: 0.2,
}

export const circleRouteOptions: google.maps.CircleOptions = {
  ...circleOptions,
  strokeColor: '#009148',
  strokeOpacity: 0.5,
  fillColor: '#009148',
}

export const circleRouteEditingOptions: google.maps.CircleOptions = {
  ...circleRouteOptions,
  strokeOpacity: 0.9,
  fillOpacity: 0.15,
}

/*
 * Polyline
 */

export const colors = [
  '#009148', // green
  '#714ca8', // purple
  '#fcba03', // gold
  '#4287f5', // blue
  '#a83291', // pink
  '#cc7b18', // orange
  '#18c3cc', // cyan
  '#18cc9c', // light green
  '#78380a', // brown
  '#5e0808', // dark red
]

export const randomColor = () => {
  return colors[Math.floor(Math.random() * colors.length)]
}

export const polylineOptions: google.maps.PolylineOptions = {
  strokeColor: '#009148',
  visible: false,
  strokeOpacity: 1,
  strokeWeight: 2,
  zIndex: 1,
  geodesic: false,
}

export const polylineEditingOptions: google.maps.PolylineOptions = {
  ...polylineOptions,
  visible: true,
}

/*
 * Drawing Manager
 */

export const drawingManagerOptions: google.maps.drawing.DrawingManagerOptions =
  {
    drawingControl: false,
    polygonOptions: polygonEditingOptions,
    circleOptions: circleEditingOptions,
  }
