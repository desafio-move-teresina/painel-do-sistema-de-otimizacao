import {
  PolygonMenuItem,
  BusStopMenuItem,
  RouteMenuItem,
} from 'utils/itemsController'
import { IPointDTO, IBusStopDTO, IMapDTO } from 'repositories/IMapsRepository'
import { IRouteDTO } from 'repositories/IRoutesRepository'

/*
 * From repository to internal
 */

/** Convert a repository polygons to a internal polygon */
export const parsePolygonFromAPI = (
  map: google.maps.Map,
  points: IPointDTO[]
): PolygonMenuItem => {
  const latLgnArray = points.map((point) => {
    return new google.maps.LatLng({ lat: point.latitude, lng: point.longitude })
  })
  const mvcArray = new google.maps.MVCArray<google.maps.LatLng>(latLgnArray)
  const polygon = new google.maps.Polygon()
  polygon.setPath(mvcArray)
  polygon.setMap(map)

  return new PolygonMenuItem(polygon)
}

/** Convert a repository bus stop to a internal bus stop */
export const parseBusStopFromAPI = (
  map: google.maps.Map,
  busStop: IBusStopDTO
): BusStopMenuItem => {
  const marker = new google.maps.Marker()
  marker.setPosition({ lat: busStop.latitude, lng: busStop.longitude })
  marker.setMap(map)

  const busStopMenuItem = new BusStopMenuItem(marker)
  busStopMenuItem.id = busStop.id
  busStopMenuItem.direction = busStop.direction
  busStopMenuItem.isActive = !!busStop.isActive
  busStopMenuItem.passengersIn = busStop.passengersIn
  busStopMenuItem.passengersOut = busStop.passengersOut

  return busStopMenuItem
}

/** Convert bus stop from repository routes array to internal bus stops */
export const parseBusStopsFromRoutesAPI = (
  map: google.maps.Map,
  routes: IRouteDTO[]
): BusStopMenuItem[] => {
  const busStopsItems = routes.reduce((currentItems, route) => {
    const newItems = route.busStops.map((busStop) =>
      parseBusStopFromAPI(map, busStop)
    )
    return [...currentItems, ...newItems]
  }, [] as BusStopMenuItem[])

  return busStopsItems
}

/** Convert a repository route to a internal route */
export const parseRouteFromAPI = (
  map: google.maps.Map,
  route: IRouteDTO,
  allBusStopItems: BusStopMenuItem[]
): RouteMenuItem => {
  const routeMenuItem = new RouteMenuItem(
    route.overviewPolyline,
    map,
    route.name,
    route.id,
    route.optimizationId
  )
  routeMenuItem.updateBusStops(route.busStops, allBusStopItems)

  return routeMenuItem
}

export const parseRoutesFromAPI = (
  map: google.maps.Map,
  routes: IRouteDTO[]
) => {
  const busStopMenuItems = parseBusStopsFromRoutesAPI(map, routes)
  const routeMenuItems = routes.map((route) =>
    parseRouteFromAPI(map, route, busStopMenuItems)
  )
  return { routeMenuItems, busStopMenuItems }
}

/**
 * Convert a repository map to a internal map
 *
 * The API only support one polygon. `areaPoints` represents points of
 * one polygon. Here in the front-end we support multiple polygons,
 * but only one will be added to the array `polygonMenuItems`
 */
export const parseMapFromAPI = (map: google.maps.Map, apiMap: IMapDTO) => {
  PolygonMenuItem.itemsCount = 1
  BusStopMenuItem.itemsCount = 1

  const { areaPoints, busStops } = apiMap
  const polygonMenuItems: PolygonMenuItem[] = []

  if (areaPoints.length > 0) {
    polygonMenuItems.push(parsePolygonFromAPI(map, areaPoints))
  }

  const busStopMenuItems = busStops.map((busStop) => {
    return parseBusStopFromAPI(map, busStop)
  })

  return { polygonMenuItems, busStopMenuItems }
}

/*
 * From internal to repository
 */

/** Convert a internal polygons to a repository polygon */
export const parsePolygonToAPI = (
  polygonMenuItem: PolygonMenuItem
): IPointDTO[] => {
  return polygonMenuItem.points
}

/** Convert a internal bus stop to a repository bus stop */
export const parseBusStopToAPI = (
  busStopMenuItem: BusStopMenuItem
): IBusStopDTO => {
  const { latitude, longitude } = busStopMenuItem.position
  // TODO: API is throwing error when passing empty array. Remove
  // this when it get fixed
  const nearbyPlaces = [{ name: 'Place Test' }]

  const busStop = {
    id: busStopMenuItem.id,
    latitude,
    longitude,
    radius: busStopMenuItem.radius,
    direction: busStopMenuItem.direction,
    isActive: busStopMenuItem.isActive ? 1 : 0,
    passengersIn: busStopMenuItem.passengersIn,
    passengersOut: busStopMenuItem.passengersOut,
    nearbyPlaces,
  }

  return busStop
}

/** Convert a internal map to a repository map */
export const parseMapToAPI = (
  polygonMenuItems: PolygonMenuItem[],
  busStopMenuItems: BusStopMenuItem[]
): IMapDTO => {
  let areaPoints: IPointDTO[] = []
  let area = 0
  if (polygonMenuItems[0]) {
    areaPoints = parsePolygonToAPI(polygonMenuItems[0])
    area = polygonMenuItems[0].area
  }

  const busStops = busStopMenuItems.map((item) => parseBusStopToAPI(item))

  return {
    title: 'Map',
    areaPoints,
    area,
    busStops,
  }
}
