import * as yup from 'yup'
import { isValid as cnpjIsValid } from '@fnando/cnpj'

export const email = yup
  .string()
  .typeError('Email inválido')
  .required('Campo obrigatório')
  .email('Email inválido')

/** Matches CNPJ in the format: 99.999.999/9999-99 */
export const cnpj = yup
  .string()
  .required('Campo obrigatório')
  .matches(/^\d{2}\.\d{3}\.\d{3}\/\d{4}-\d{2}$/, 'CNPJ inválido')
  .test('isCnpj', 'CNPJ inválido', (value) => cnpjIsValid(value ? value : ''))

/** Matches phones in the format: (99) 9999-9999 */
export const phoneNumber = yup
  .string()
  .required('Campo obrigatório')
  .matches(/^\(\d{2}\)\s\d{4}-\d{4}$/, 'Telefone inválido')

/** 7 characters for the license plate */
export const licensePlate = yup
  .string()
  .required('Campo obrigatório')
  .min(8, 'Placa inválida')
  .max(8, 'Placa inválida')

export const defaultString = yup
  .string()
  .typeError('Deve ser texto')
  .required('Campo obrigatório')

export const defaultAddress = yup
  .string()
  .typeError('Deve ser texto')
  .required('Campo obrigatório')
  .min(4, 'Digite o endereço completo')
  .max(120, 'Endereço muito longo')

export const defaultIntegerRule = yup
  .number()
  .typeError('Deve ser um número inteiro')
  .required('Campo obrigatório')
  .integer('Deve ser um número inteiro')
  .min(1, 'O valor mínimo é 1')

export const defaultAngleRule = yup
  .number()
  .typeError('Deve ser um número inteiro de 0 à 359')
  .required('Campo obrigatório')
  .integer('Deve ser um número inteiro')
  .min(0, 'O valor mínimo é 0')
  .max(359, 'O valor máximo é 359')

export const defaultBooleanRule = yup
  .boolean()
  .typeError('Deve ser verdadeiro ou falso')

export const defaultName = yup
  .string()
  .typeError('Deve ser texto')
  .required('Campo obrigatório')
  .min(3, 'Digite o nome completo')
  .max(80, 'Nome muito longo')
