export interface IPointDTO {
  latitude: number
  longitude: number
}

export interface IBusStopDTO {
  id?: string
  latitude: number
  longitude: number
  radius: number
  direction: number
  isActive: number
  passengersIn: number
  passengersOut: number
  nearbyPlaces?: { name: string }[]
}

export interface IMapDTO {
  /** Map title */
  title: string

  /** Holds the points of one polygon */
  areaPoints: IPointDTO[]

  /** The polygon (areaPoints) area in square meters */
  area: number

  /** Holds all bus stops on the map  */
  busStops: IBusStopDTO[]
}

export interface IMapsRepository {
  get(): Promise<IMapDTO>
  create(map: IMapDTO): Promise<IMapDTO>
  areaBusStops(areaPoints: IPointDTO[]): Promise<IBusStopDTO[]>
  process(routesCount: number): Promise<void>
}
