export interface ICompanyDTO {
  id: string
  cnpj: string
  name: string
  consortium: string
  phone: string
  email: string
  address: string
}

export type ICompanyCreateDTO = Omit<ICompanyDTO, 'id'>

export interface ICompaniesRepository {
  list(): Promise<ICompanyDTO[]>
  get(id: string): Promise<ICompanyDTO>
  create(company: ICompanyCreateDTO): Promise<ICompanyDTO>
  edit(id: string, company: ICompanyDTO): Promise<ICompanyDTO>
  delete(id: string): Promise<void>
}
