import { ICompanyDTO } from './ICompaniesRepository'

export type VehicleStatus = 'active' | 'sold' | 'maintenance'

export const vehicleStatusText = {
  active: 'Ativo',
  sold: 'Vendido',
  maintenance: 'Manutenção',
}

export type VehicleType =
  | 'micro'
  | 'mini'
  | 'mid'
  | 'basic'
  | 'standard'
  | 'articulated'
  | 'bi-articulated'

export interface IVehicleDTO {
  id: string
  companyId: string
  company: ICompanyDTO
  licencePlate: string
  manufactureYear: number
  status: VehicleStatus
  prefix: string
  seatedCapacity: number
  totalCapacity: number
  hasElevator: boolean
  vehicleType: VehicleType
}

export type IVehicleCreateDTO = Omit<IVehicleDTO, 'id' | 'company'>

export interface IVehiclesRepository {
  list(companyId?: string, status?: string): Promise<IVehicleDTO[]>
  get(id: string): Promise<IVehicleDTO>
  create(vehicle: IVehicleCreateDTO): Promise<IVehicleDTO>
  edit(id: string, vehicle: IVehicleCreateDTO): Promise<IVehicleDTO>
  delete(id: string): Promise<void>
}
