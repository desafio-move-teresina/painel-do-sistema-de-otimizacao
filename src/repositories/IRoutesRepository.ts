import { IBusStopDTO } from './IMapsRepository'

export interface IRouteVehicleDTO {
  id: string
  totalCapacity: number
}

export interface IRouteStepDTO {
  id: string
  distance: number
  duration: number
  htmlInstruction: string
}

export interface IRouteLegDTO {
  id: string
  routeId: string
  distance: number
  duration: number
  steps: IRouteStepDTO[]
}

export interface IRouteDTO {
  id: string
  name: string
  optimizationId: string
  busStops: IBusStopDTO[]
  overviewPolyline: string
  estimatedPassengers: number
  estimatedDistance: number
  estimatedDuration: number
  routeVehicles: IRouteVehicleDTO[]
  routeLegs: IRouteLegDTO[]
}

export interface IOptimizationDTO {
  id: string
  name: string
}

export function getSteps({ routeLegs }: IRouteDTO) {
  const steps = routeLegs.reduce(
    (steps: IRouteStepDTO[], leg) => steps.concat(leg.steps),
    []
  )
  return steps
}

export interface IRoutesRepository {
  list(): Promise<IRouteDTO[]>
  reorder(id: string): Promise<IRouteDTO>
  optimizations(): Promise<IOptimizationDTO[]>
}
