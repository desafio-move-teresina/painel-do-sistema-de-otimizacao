export interface IDecisionVariablesDTO {
  stopMinimumDistance: number
  stopMaximumDistance: number
  maximumWait: number
  defaultBusStopRadius: number
}

export interface IDecisionVariablesRepository {
  get(): Promise<IDecisionVariablesDTO | null>
  create(
    decisionVariables: IDecisionVariablesDTO
  ): Promise<IDecisionVariablesDTO>
}
