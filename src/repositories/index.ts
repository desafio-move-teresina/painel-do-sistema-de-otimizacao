import { DecisionVariablesRepository } from './implementations/DecisionVariablesRepository'
import { CompaniesRepository } from './implementations/CompaniesRepository'
import { VehiclesRepository } from './implementations/VehiclesRepository'
import { MapsRepository } from './implementations/MapsRepository'
import { PlacesRepository } from './implementations/PlacesRepository'
import { RoutesRepository } from './implementations/RoutesRepository'

// TODO: instead of importing this file, refactor to use dependency injection

export const decisionVariablesRepository = new DecisionVariablesRepository()
export const companiesRepository = new CompaniesRepository()
export const vehiclesRepository = new VehiclesRepository()
export const mapsRepository = new MapsRepository()
export const placesRepository = new PlacesRepository()
export const routesRepository = new RoutesRepository()
