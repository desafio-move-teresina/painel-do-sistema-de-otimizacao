import { IPointDTO } from './IMapsRepository'

export interface IPlaceDTO {
  id: string
  name: string
}

export interface IPlacesRepository {
  list(
    location: IPointDTO,
    radius: number,
    map: google.maps.Map
  ): Promise<IPlaceDTO[]>
}
