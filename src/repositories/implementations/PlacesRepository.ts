import { IPointDTO } from 'repositories/IMapsRepository'
import { IPlacesRepository, IPlaceDTO } from 'repositories/IPlacesRepository'

export class PlacesRepository implements IPlacesRepository {
  list(
    { latitude, longitude }: IPointDTO,
    radius: number,
    map: google.maps.Map
  ): Promise<IPlaceDTO[]> {
    const places = new google.maps.places.PlacesService(map)

    const params = {
      location: { lat: latitude, lng: longitude },
      radius,
      type: 'point_of_interest',
    }

    const promise = new Promise<IPlaceDTO[]>((resove) => {
      places.nearbySearch(params, (googlePlaces) => {
        if (!googlePlaces) return resove([])

        const places = googlePlaces.map((googlePlace) => {
          return {
            id: googlePlace.place_id || '0',
            name: googlePlace.name || 'Desconhecido',
          }
        })
        return resove(places)
      })
    })

    return promise
  }
}
