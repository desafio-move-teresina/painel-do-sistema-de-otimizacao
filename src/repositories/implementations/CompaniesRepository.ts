import { api } from 'services/api'
import {
  ICompanyDTO,
  ICompanyCreateDTO,
  ICompaniesRepository,
} from 'repositories/ICompaniesRepository'

export class CompaniesRepository implements ICompaniesRepository {
  async list(): Promise<ICompanyDTO[]> {
    const { data: companies } = await api.get('/companies')
    return companies
  }

  async get(id: string): Promise<ICompanyDTO> {
    const { data: company } = await api.get(`/companies/${id}`)
    return company
  }

  async create(company: ICompanyCreateDTO): Promise<ICompanyDTO> {
    const { data: newCompany } = await api.post('/companies', company)
    return newCompany
  }

  async edit(id: string, company: ICompanyDTO): Promise<ICompanyDTO> {
    const { data: editedCompany } = await api.put(`/companies/${id}`, company)
    return editedCompany
  }

  async delete(id: string): Promise<void> {
    await api.delete(`/companies/${id}`)
  }
}
