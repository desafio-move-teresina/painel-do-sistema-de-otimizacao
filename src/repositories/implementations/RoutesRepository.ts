import { api } from 'services/api'
import { IRouteDTO, IOptimizationDTO } from 'repositories/IRoutesRepository'

export class RoutesRepository {
  async list(): Promise<IRouteDTO[]> {
    const { data: _routes } = await api.get('/routes')

    // eslint-disable-next-line
    const routes = _routes.map((route: any) => {
      // The API is not returning theses 3 properties in camel case
      const busStops = route.busstops
      const routeVehicles = route.routevehicles
      const routeLegs = route.routelegs

      delete route.busstops
      delete route.routevehicles
      delete route.routelegs

      return {
        ...route,
        busStops,
        routeVehicles,
        routeLegs,
      }
    })

    return routes
  }

  async reorder(id: string): Promise<IRouteDTO> {
    const params = { route: id }
    // API should probably use kebab-case (ex: /reorder-route)
    const { data: route } = await api.get('/reorderSingleRoute', { params })
    return route
  }

  async optimizations(): Promise<IOptimizationDTO[]> {
    const { data: optimizations } = await api.get('/optimization')
    return optimizations
  }
}
