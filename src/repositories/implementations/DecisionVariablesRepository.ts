import { api } from 'services/api'
import {
  IDecisionVariablesDTO,
  IDecisionVariablesRepository,
} from 'repositories/IDecisionVariables'

export class DecisionVariablesRepository
  implements IDecisionVariablesRepository
{
  async get(): Promise<IDecisionVariablesDTO | null> {
    const { data: decisionVariables } = await api.get('/decision-variables')

    // API gives an empty object when there is no decision variables
    if (typeof decisionVariables?.stopMinimumDistance !== 'number') {
      return null
    }

    return decisionVariables
  }

  async create(
    decisionVariables: IDecisionVariablesDTO
  ): Promise<IDecisionVariablesDTO> {
    const { data: newDecisionVariables } = await api.post(
      '/decision-variables',
      decisionVariables
    )
    return newDecisionVariables
  }
}
