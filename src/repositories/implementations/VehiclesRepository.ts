import { api } from 'services/api'
import {
  IVehicleDTO,
  IVehicleCreateDTO,
  IVehiclesRepository,
} from 'repositories/IVehiclesRepository'

export class VehiclesRepository implements IVehiclesRepository {
  async list(companyId?: string, status?: string): Promise<IVehicleDTO[]> {
    const params: { company?: string; status?: string } = {}

    if (companyId) params.company = companyId
    if (status) params.status = status

    const { data: _vehicles } = await api.get('/vehicles', { params })

    // The API is using 0 or 1 for the property hasElevator. Converting it to
    // boolean
    const vehicles = _vehicles.map((vehicle: IVehicleDTO) => {
      vehicle.hasElevator = !!vehicle.hasElevator
      return vehicle
    })

    return vehicles
  }

  async get(id: string): Promise<IVehicleDTO> {
    const { data: vehicle } = await api.get(`/vehicles/${id}`)
    vehicle.hasElevator = !!vehicle.hasElevator

    return vehicle
  }

  async create(vehicle: IVehicleCreateDTO): Promise<IVehicleDTO> {
    const { data: newVehicle } = await api.post('/vehicles', vehicle)
    return newVehicle
  }

  async edit(id: string, vehicle: IVehicleCreateDTO): Promise<IVehicleDTO> {
    const { data: editedVehicle } = await api.put(`/vehicles/${id}`, vehicle)
    return editedVehicle
  }

  async delete(id: string): Promise<void> {
    await api.delete(`/vehicles/${id}`)
  }
}
