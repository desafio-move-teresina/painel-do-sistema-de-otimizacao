import { api } from 'services/api'
import {
  IPointDTO,
  IBusStopDTO,
  IMapDTO,
  IMapsRepository,
} from 'repositories/IMapsRepository'

export class MapsRepository implements IMapsRepository {
  async get(): Promise<IMapDTO> {
    const { data: map } = await api.get('/map-editor')

    // API gives an empty array when there is no map
    if (Array.isArray(map)) {
      return {
        title: 'Map',
        areaPoints: [],
        area: 0,
        busStops: [],
      }
    }

    return map
  }

  async create(map: IMapDTO): Promise<IMapDTO> {
    const { data: newMap } = await api.post('/map-editor', map)
    return newMap
  }

  async areaBusStops(areaPoints: IPointDTO[]): Promise<IBusStopDTO[]> {
    const { data: busStops } = await api.post(
      '/getBusStopsInsidePolygon',
      areaPoints
    )
    return busStops
  }

  async process(routesCount: number): Promise<void> {
    await api.post('/map-process', { routesCount })
  }
}
