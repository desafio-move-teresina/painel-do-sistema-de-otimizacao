import { ReactNode } from 'react'
import Head from 'next/head'

import { Header } from 'components/Header'
import { Navbar } from 'components/Navbar'
import styles from './styles.module.scss'

interface PageLayoutProps {
  headTitle: string
  title?: string
  className?: string
  children: ReactNode
}

export function PageLayout({
  headTitle,
  title,
  children,
  className,
}: PageLayoutProps) {
  return (
    <>
      <Head>
        <title>{headTitle}</title>
      </Head>

      <Navbar />

      <Header />

      <div className={styles.contentContainer}>
        {title && <h1>{title}</h1>}

        <section className={className}>{children}</section>
      </div>
    </>
  )
}
