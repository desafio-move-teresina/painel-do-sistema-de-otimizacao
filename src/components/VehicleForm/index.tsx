import { useEffect } from 'react'
import { useRouter } from 'next/router'
import { useQuery } from 'react-query'
import { SubmitHandler, useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup/dist/yup'
import * as yup from 'yup'

import {
  IVehicleDTO,
  IVehicleCreateDTO,
  VehicleStatus,
  VehicleType,
} from 'repositories/IVehiclesRepository'
import { companiesRepository } from 'repositories'
import {
  licensePlate,
  defaultIntegerRule,
  defaultString,
  defaultBooleanRule,
} from 'utils/validation'
import styles from './styles.module.scss'

type VehicleFormData = {
  companyId: string
  // FIXME: fix typo, it should be licensePlate
  licencePlate: string
  manufactureYear: number
  status: VehicleStatus
  prefix: string
  seatedCapacity: number
  totalCapacity: number
  hasElevator: boolean
  vehicleType: VehicleType
}

const vehicleFormSchema = yup.object().shape({
  companyId: defaultString,
  licencePlate: licensePlate,
  manufactureYear: defaultIntegerRule,
  status: defaultString,
  prefix: defaultString,
  seatedCapacity: defaultIntegerRule,
  totalCapacity: defaultIntegerRule,
  hasElevator: defaultBooleanRule,
  vehicleType: defaultString,
})

interface VehicleFormProps {
  title: string
  vehicle?: IVehicleDTO
  onSubmit: (values: IVehicleCreateDTO) => Promise<void>
}

export function VehicleForm({ title, vehicle, onSubmit }: VehicleFormProps) {
  const { register, handleSubmit, formState, setValue } =
    useForm<VehicleFormData>({
      resolver: yupResolver(vehicleFormSchema),
    })

  // Fetch companies for the select input
  const query = useQuery('companies', companiesRepository.list)
  const { data: companies } = query

  const { errors } = formState
  const router = useRouter()

  const _onSubmit: SubmitHandler<VehicleFormData> = (values) => {
    onSubmit(values)
  }

  useEffect(() => {
    if (vehicle) {
      const formStateClean = !formState.isDirty
      if (formStateClean) {
        setValue('companyId', vehicle.companyId)
        setValue('licencePlate', vehicle.licencePlate)
        setValue('manufactureYear', vehicle.manufactureYear)
        setValue('status', vehicle.status)
        setValue('prefix', vehicle.prefix)
        setValue('seatedCapacity', vehicle.seatedCapacity)
        setValue('totalCapacity', vehicle.totalCapacity)
        setValue('hasElevator', vehicle.hasElevator)
        setValue('vehicleType', vehicle.vehicleType)
      }
      return
    }
  }, [vehicle, formState.isDirty, setValue])

  return (
    <form className={styles.vehicleForm} onSubmit={handleSubmit(_onSubmit)}>
      <h2>{title}</h2>

      <fieldset>
        <label htmlFor="licencePlate">Placa:</label>
        <input
          id="licencePlate"
          placeholder="AAA-9999"
          maxLength={8}
          {...register('licencePlate')}
        />
        <span>{errors.licencePlate?.message}</span>
      </fieldset>

      <fieldset>
        <label htmlFor="manufactureYear">Ano de fabricação:</label>
        <input
          id="manufactureYear"
          placeholder="2020"
          maxLength={4}
          {...register('manufactureYear')}
        />
        <span>{errors.manufactureYear?.message}</span>
      </fieldset>

      <fieldset>
        <label htmlFor="prefix">Prefixo:</label>
        <input
          id="prefix"
          placeholder="9999"
          maxLength={4}
          {...register('prefix')}
        />
        <span>{errors.prefix?.message}</span>
      </fieldset>

      <fieldset>
        <label htmlFor="companyId">Empresa:</label>
        <select id="companyId" {...register('companyId')}>
          {companies &&
            companies.map((company) => (
              <option key={company.id} value={company.id}>
                {company.name}
              </option>
            ))}
        </select>
        <span>{errors.companyId?.message}</span>
      </fieldset>

      <fieldset>
        <label htmlFor="seatedCapacity">
          Capacidade de passageiros sentados:
        </label>
        <input
          id="seatedCapacity"
          type="number"
          placeholder="30"
          {...register('seatedCapacity')}
        />
        <span>{errors.seatedCapacity?.message}</span>
      </fieldset>

      <fieldset>
        <label htmlFor="totalCapacity">Capacidade total de passageiros:</label>
        <input
          id="totalCapacity"
          type="number"
          placeholder="50"
          {...register('totalCapacity')}
        />
        <span>{errors.totalCapacity?.message}</span>
      </fieldset>

      <fieldset>
        <label htmlFor="status">Situação:</label>
        <select id="status" {...register('status')}>
          <option value="active">Ativo</option>
          <option value="sold">Vendido</option>
          <option value="maintenance">Manutenção</option>
        </select>
        <span>{errors.status?.message}</span>
      </fieldset>

      <fieldset className={styles.checkBoxContainer}>
        <input
          type="checkbox"
          id="hasElevator"
          defaultChecked={vehicle?.hasElevator || false}
          {...register('hasElevator')}
        />
        <label htmlFor="hasElevator">Tem elevador</label>
        <span>{errors.hasElevator?.message}</span>
      </fieldset>

      <fieldset>
        <label htmlFor="vehicleType">Tipo de veículo:</label>
        <select id="vehicleType" {...register('vehicleType')}>
          <option value="micro">Micro ônibus</option>
          <option value="mini">Mini ônibus</option>
          <option value="mid">Midi ônibus</option>
          <option value="basic">Ônibus básico</option>
          <option value="standard">Ônibus padron</option>
          <option value="articulated">Ônibus articulado</option>
          <option value="bi-articulated">Ônibus biarticulado</option>
        </select>
        <span>{errors.vehicleType?.message}</span>
      </fieldset>

      <div>
        <button type="button" onClick={() => router.back()}>
          Cancelar
        </button>
        <button type="submit">Salvar</button>
      </div>
    </form>
  )
}
