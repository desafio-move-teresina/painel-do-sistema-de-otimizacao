import { useEffect } from 'react'
import { useRouter } from 'next/router'
import { SubmitHandler, useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup/dist/yup'
import * as yup from 'yup'
import { cnpjMask, phoneMask } from 'utils/inputMask'

import { ICompanyCreateDTO } from 'repositories/ICompaniesRepository'
import {
  email,
  cnpj,
  phoneNumber,
  defaultAddress,
  defaultName,
} from 'utils/validation'
import styles from './styles.module.scss'

type CompanyFormData = {
  cnpj: string
  name: string
  consortium: string
  phone: string
  email: string
  address: string
}

const companyFormSchema = yup.object().shape({
  cnpj: cnpj,
  name: defaultName,
  consortium: defaultName,
  phone: phoneNumber,
  email: email,
  address: defaultAddress,
})

interface CompanyFormProps {
  title: string
  company?: ICompanyCreateDTO
  onSubmit: (values: ICompanyCreateDTO) => Promise<void>
}

export function CompanyForm({ title, company, onSubmit }: CompanyFormProps) {
  const { register, handleSubmit, formState, setValue } =
    useForm<CompanyFormData>({
      resolver: yupResolver(companyFormSchema),
    })

  const { errors } = formState
  const router = useRouter()

  const _onSubmit: SubmitHandler<CompanyFormData> = (values) => {
    onSubmit(values)
  }

  const cnpjReg = { ...register('cnpj') }
  const phoneReg = { ...register('phone') }

  useEffect(() => {
    if (company) {
      const formStateClean = !formState.isDirty
      if (formStateClean) {
        setValue('cnpj', company.cnpj)
        setValue('name', company.name)
        setValue('consortium', company.consortium)
        setValue('phone', company.phone)
        setValue('email', company.email)
        setValue('address', company.address)
      }
      return
    }
  }, [company, formState.isDirty, setValue])

  return (
    <form className={styles.companyForm} onSubmit={handleSubmit(_onSubmit)}>
      <h2>{title}</h2>
      <fieldset>
        <label htmlFor="cnpj">CNPJ:</label>
        <input
          id="cnpj"
          placeholder="99.999.999/9999-99"
          {...register('cnpj')}
          onChange={(event) => {
            event.currentTarget.value = cnpjMask(event.currentTarget.value)
            cnpjReg.onChange(event)
          }}
        />
        <span>{errors.cnpj?.message}</span>
      </fieldset>

      <fieldset>
        <label htmlFor="name">Nome da empresa:</label>
        <input id="name" placeholder="Nome" {...register('name')} />
        <span>{errors.name?.message}</span>
      </fieldset>

      <fieldset>
        <label htmlFor="consortium">Nome do Consórcio:</label>
        <input
          id="consortium"
          placeholder="Consórcio"
          {...register('consortium')}
        />
        <span>{errors.consortium?.message}</span>
      </fieldset>

      <fieldset>
        <label htmlFor="phone">Telefone:</label>
        <input
          id="phone"
          placeholder="(99) 99999-9999"
          {...register('phone')}
          onChange={(event) => {
            event.currentTarget.value = phoneMask(event.currentTarget.value)
            phoneReg.onChange(event)
          }}
        />
        <span>{errors.phone?.message}</span>
      </fieldset>

      <fieldset>
        <label htmlFor="email">Email:</label>
        <input
          id="email"
          placeholder="empresa@mail.com"
          {...register('email')}
        />
        <span>{errors.email?.message}</span>
      </fieldset>

      <fieldset>
        <label htmlFor="address">Endereço:</label>
        <input id="address" placeholder="Endereço" {...register('address')} />
        <span>{errors.address?.message}</span>
      </fieldset>

      <div>
        <button type="button" onClick={() => router.back()}>
          Cancelar
        </button>
        <button type="submit">Salvar</button>
      </div>
    </form>
  )
}
