import Modal from 'react-modal'
import { useQuery } from 'react-query'
import { IoMdClose as CloseIcon } from 'react-icons/io'

import { BusStopMenuItem } from 'utils/itemsController'
import { placesRepository } from 'repositories'
import styles from './styles.module.scss'

interface PlacesModalProps {
  busStopMenuItem: BusStopMenuItem
  isModalOpen: boolean
  setIsModalOpen: (isModalOpen: boolean) => void
}

// The time (in milliseconds) that react-query will consider
// the cache expired. Set to 3 minutes
const staleTime = 1000 * 60 * 3

export function PlacesModal({
  busStopMenuItem,
  isModalOpen,
  setIsModalOpen,
}: PlacesModalProps) {
  const { position, radius, map } = busStopMenuItem

  const query = useQuery(
    ['places', position],
    () => placesRepository.list(position, radius, map as google.maps.Map),
    { enabled: map instanceof google.maps.Map, retry: false, staleTime }
  )

  const { data } = query

  /** Close this modal */
  const onRequestClose = () => {
    setIsModalOpen(false)
  }

  return (
    <Modal
      isOpen={isModalOpen}
      onRequestClose={onRequestClose}
      overlayClassName="reactModalOverlay"
      className="reactModalContent"
    >
      <button
        type="button"
        onClick={onRequestClose}
        className="reactModalClose"
      >
        <CloseIcon />
      </button>

      <div className={styles.container}>
        <h2>Locais Próximos</h2>

        <ul>
          {data && data.map((place) => <li key={place.id}>{place.name}</li>)}
        </ul>

        {data?.length === 0 && <p>Nenhum local encontrado nas proximidades.</p>}
      </div>
    </Modal>
  )
}
