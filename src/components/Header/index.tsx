import Link from 'next/link'
import { BsSearch as SearchIcon } from 'react-icons/bs'
import { FaBell as BellIcon } from 'react-icons/fa'

import styles from './styles.module.scss'

export function Header() {
  return (
    <header className={styles.headerContainer}>
      <div className={styles.headerContent}>
        <nav>
          <Link href="/">
            <a>Sistema de geração de rotas - Prefeitura de Teresina</a>
          </Link>
        </nav>

        <nav>
          <button>
            <SearchIcon />
          </button>

          <button>
            <BellIcon />
          </button>

          <button>
            <span>Maria Fernanda</span>
            <img src="/images/profile-image.png" alt="Imagem de perfil" />
          </button>
        </nav>
      </div>
    </header>
  )
}
