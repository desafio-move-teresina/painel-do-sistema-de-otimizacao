import Link from 'next/link'
import { BsPieChartFill as ChartIcon } from 'react-icons/bs'
import { MdSettings as SettingsIcon } from 'react-icons/md'
import { BsFillHouseDoorFill as CompanyIcon } from 'react-icons/bs'
import { FaBus as BusIcon } from 'react-icons/fa'
import { FaMap as MapIcon } from 'react-icons/fa'
import { RiShapeFill as RouteIcon } from 'react-icons/ri'
import { IoMdDocument as ReportIcon } from 'react-icons/io'

import { ActiveLink } from 'components/ActiveLink'
import styles from './styles.module.scss'

export function Navbar() {
  return (
    <div className={styles.navbar}>
      <header>
        <Link href="/">
          <a>
            <img src="/images/logo.jpg" alt="Prefeitura de Teresina" />
          </a>
        </Link>
      </header>

      <nav className={styles.navigation}>
        <ActiveLink href="/" activeClassName={styles.active}>
          <a>
            <ChartIcon />
            <span>Dashboard</span>
          </a>
        </ActiveLink>

        <ActiveLink href="/settings" activeClassName={styles.active}>
          <a>
            <SettingsIcon />
            <span>Configurações</span>
          </a>
        </ActiveLink>

        <ActiveLink href="/companies" activeClassName={styles.active}>
          <a>
            <CompanyIcon />
            <span>Empresas</span>
          </a>
        </ActiveLink>

        <ActiveLink href="/vehicles" activeClassName={styles.active}>
          <a>
            <BusIcon />
            <span>Veículos</span>
          </a>
        </ActiveLink>

        <ActiveLink href="/map-editor" activeClassName={styles.active}>
          <a>
            <MapIcon />
            <span>Mapa</span>
          </a>
        </ActiveLink>

        <ActiveLink href="/routes" activeClassName={styles.active}>
          <a>
            <RouteIcon />
            <span>Rotas</span>
          </a>
        </ActiveLink>

        <ActiveLink href="/reports" activeClassName={styles.active}>
          <a>
            <ReportIcon />
            <span>Relatórios</span>
          </a>
        </ActiveLink>
      </nav>
    </div>
  )
}
