import { useState, MouseEvent, ChangeEvent } from 'react'
import { MdPlace as PlacesIcon } from 'react-icons/md'
import { MdModeEdit as EditIcon } from 'react-icons/md'
import { MdDelete as DeleteIcon } from 'react-icons/md'

import { SelectedRoute } from 'pages/routes'
import { MenuItem, BusStopMenuItem, RouteMenuItem } from 'utils/itemsController'
import styles from './styles.module.scss'
import { IOptimizationDTO } from 'repositories/IRoutesRepository'

interface MenuBodyProps {
  onRouteReprocessClick: (routeId?: string) => void
  editableMenuItem: MenuItem | null
  busStopMenuItems: BusStopMenuItem[]
  routeMenuItems: RouteMenuItem[]
  selectedRoute: SelectedRoute
  changeSelectedRoute: (route: SelectedRoute) => void
  optimizations: IOptimizationDTO[]
  toggleMenuItemEditable: (menuItem: MenuItem) => void
  deleteBusStopMenuItem: (deletedBusStopItem: BusStopMenuItem) => void
  onOpenPlacesModal: (event: MouseEvent<HTMLElement>) => void
  onOpenEditBusStopModal: (event: MouseEvent<HTMLElement>) => void
}

export function MenuBody({
  onRouteReprocessClick,
  editableMenuItem,
  busStopMenuItems,
  routeMenuItems,
  selectedRoute,
  changeSelectedRoute,
  optimizations,
  toggleMenuItemEditable,
  deleteBusStopMenuItem,
  onOpenPlacesModal,
  onOpenEditBusStopModal,
}: MenuBodyProps) {
  const [optimizationSelectValue, setOptimizationSelectValue] = useState('all')

  const activeRoute =
    selectedRoute instanceof RouteMenuItem ? selectedRoute : null

  const routesSelectValue =
    selectedRoute instanceof RouteMenuItem
      ? selectedRoute.id || 'none'
      : selectedRoute

  // Routes filtered by the selected optimization
  const filteredRouteItems = routeMenuItems.filter(
    (routeItem) =>
      optimizationSelectValue === 'all' ||
      routeItem.optimizationId === optimizationSelectValue
  )

  let busStopItems: BusStopMenuItem[] | null = null

  if (selectedRoute === 'all') {
    busStopItems = busStopMenuItems
  } else if (selectedRoute instanceof RouteMenuItem) {
    busStopItems = selectedRoute.busStopMenuItems
  }

  const onChangeSelectedOptimization = (
    event: ChangeEvent<HTMLSelectElement>
  ) => {
    const selected: string = event.target.value

    if (selected !== optimizationSelectValue) {
      changeSelectedRoute('none')
      setOptimizationSelectValue(selected)
    }
  }

  const onChangeSelectedRoute = (event: ChangeEvent<HTMLSelectElement>) => {
    const selected: string = event.target.value

    if (selected === 'all') {
      changeSelectedRoute(selected)
      setOptimizationSelectValue('all')
    } else if (selected === 'none') {
      changeSelectedRoute(selected)
    } else {
      const route = routeMenuItems.find((route) => route.id === selected)
      changeSelectedRoute(route || 'none')
    }
  }

  return (
    <div className={styles.menuBody}>
      <h4>Selecionar Mapa</h4>
      <div>
        <select
          value={optimizationSelectValue}
          onChange={onChangeSelectedOptimization}
        >
          <option value="all">Todos</option>
          {optimizations.map((optimization) => (
            <option key={optimization.id} value={optimization.id}>
              {optimization.name}
            </option>
          ))}
        </select>
      </div>

      <h4>Selecionar Rota</h4>
      <div>
        <select value={routesSelectValue} onChange={onChangeSelectedRoute}>
          <option value="none">Selecionar</option>
          <option value="all">Todas</option>
          {filteredRouteItems.map((routeItem) => (
            <option key={routeItem.id} value={routeItem.id}>
              {routeItem.name}
            </option>
          ))}
        </select>
      </div>

      <h4>
        Pontos de ônibus
        {busStopItems && busStopItems.length > 0
          ? ` (${busStopItems.length})`
          : null}
      </h4>

      {busStopItems && busStopItems.length > 0 ? (
        <ul>
          {busStopItems.map((menuBusStop) => (
            <li
              key={menuBusStop.name}
              onMouseEnter={() => menuBusStop.onMouseEnter()}
              onMouseLeave={() => menuBusStop.onMouseLeave()}
              onClick={() => toggleMenuItemEditable(menuBusStop)}
              className={
                menuBusStop === editableMenuItem ? styles.itemActive : ''
              }
            >
              <span>{menuBusStop.name}</span>
              <span>
                <button onClick={onOpenPlacesModal}>
                  <PlacesIcon />
                </button>

                <button onClick={onOpenEditBusStopModal}>
                  <EditIcon />
                </button>

                <button
                  onClick={(event) => {
                    event.stopPropagation()
                    event.nativeEvent.stopImmediatePropagation()
                    deleteBusStopMenuItem(menuBusStop)
                  }}
                >
                  <DeleteIcon />
                </button>
              </span>
            </li>
          ))}

          {activeRoute && (
            <button
              type="button"
              onClick={() => onRouteReprocessClick(activeRoute.id)}
              className={styles.reprocessButton}
            >
              Reprocessar Rota
            </button>
          )}
        </ul>
      ) : (
        <p>(vazio)</p>
      )}
    </div>
  )
}
