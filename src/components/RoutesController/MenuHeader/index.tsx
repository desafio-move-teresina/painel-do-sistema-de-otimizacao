import { useRouter } from 'next/router'
import { BiArrowBack as BackIcon } from 'react-icons/bi'
import { FaSave as SaveIcon } from 'react-icons/fa'

import styles from './styles.module.scss'

interface MenuHeaderProps {
  onSaveClick: () => void
}

export function MenuHeader({ onSaveClick }: MenuHeaderProps) {
  /** Control back button press */
  const router = useRouter()

  return (
    <header className={styles.menuHeader}>
      <button onClick={() => router.back()}>
        <BackIcon />
      </button>

      <span>
        <button onClick={onSaveClick}>
          <SaveIcon />
        </button>
      </span>
    </header>
  )
}
