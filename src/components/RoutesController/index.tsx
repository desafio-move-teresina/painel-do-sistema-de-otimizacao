import { MouseEvent } from 'react'

import { SelectedRoute } from 'pages/routes'
import { MenuItem, BusStopMenuItem, RouteMenuItem } from 'utils/itemsController'
import { MenuHeader } from './MenuHeader'
import { MenuBody } from './MenuBody'
import { PlacesModal } from 'components/PlacesModal'
import { EditBusStopModal } from 'components/EditBusStopModal'
import styles from './styles.module.scss'
import { IOptimizationDTO } from 'repositories/IRoutesRepository'

interface RoutesControllerProps {
  onSaveClick: () => void
  onRouteReprocessClick: (routeId?: string) => void
  editableMenuItem: MenuItem | null
  busStopMenuItems: BusStopMenuItem[]
  routeMenuItems: RouteMenuItem[]
  selectedRoute: SelectedRoute
  changeSelectedRoute: (route: SelectedRoute) => void
  optimizations: IOptimizationDTO[]
  toggleMenuItemEditable: (menuItem: MenuItem) => void
  updateBusStopMenuItem: () => void
  deleteBusStopMenuItem: (deletedBusStopItem: BusStopMenuItem) => void
  isPlacesModalOpen: boolean
  setIsPlacesModalOpen: (isOpen: boolean) => void
  isEditBusStopModalOpen: boolean
  setIsEditBusStopModalOpen: (isOpen: boolean) => void
}

export function RoutesController({
  onSaveClick,
  onRouteReprocessClick,
  editableMenuItem,
  busStopMenuItems,
  routeMenuItems,
  selectedRoute,
  changeSelectedRoute,
  optimizations,
  toggleMenuItemEditable,
  updateBusStopMenuItem,
  deleteBusStopMenuItem,
  isPlacesModalOpen,
  setIsPlacesModalOpen,
  isEditBusStopModalOpen,
  setIsEditBusStopModalOpen,
}: RoutesControllerProps) {
  /** Opens the places modal */
  const onOpenPlacesModal = (event: MouseEvent<HTMLElement>) => {
    event.stopPropagation()
    event.nativeEvent.stopImmediatePropagation()
    setIsPlacesModalOpen(true)
  }

  /** Opens the edit bus stop modal */
  const onOpenEditBusStopModal = (event: MouseEvent<HTMLElement>) => {
    event.stopPropagation()
    event.nativeEvent.stopImmediatePropagation()
    setIsEditBusStopModalOpen(true)
  }

  return (
    <aside className={styles.mapEditor}>
      <MenuHeader onSaveClick={onSaveClick} />

      <div className={styles.menuBodyContainer}>
        <MenuBody
          onRouteReprocessClick={onRouteReprocessClick}
          editableMenuItem={editableMenuItem}
          busStopMenuItems={busStopMenuItems}
          routeMenuItems={routeMenuItems}
          selectedRoute={selectedRoute}
          changeSelectedRoute={changeSelectedRoute}
          optimizations={optimizations}
          toggleMenuItemEditable={toggleMenuItemEditable}
          deleteBusStopMenuItem={deleteBusStopMenuItem}
          onOpenPlacesModal={onOpenPlacesModal}
          onOpenEditBusStopModal={onOpenEditBusStopModal}
        />
      </div>

      {editableMenuItem instanceof BusStopMenuItem && (
        <PlacesModal
          busStopMenuItem={editableMenuItem}
          isModalOpen={isPlacesModalOpen}
          setIsModalOpen={setIsPlacesModalOpen}
        />
      )}

      {editableMenuItem instanceof BusStopMenuItem && (
        <EditBusStopModal
          busStopMenuItem={editableMenuItem}
          updateBusStopMenuItem={updateBusStopMenuItem}
          isModalOpen={isEditBusStopModalOpen}
          setIsModalOpen={setIsEditBusStopModalOpen}
        />
      )}
    </aside>
  )
}
