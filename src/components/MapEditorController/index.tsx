import { MouseEvent } from 'react'

import {
  MenuItem,
  PolygonMenuItem,
  BusStopMenuItem,
} from 'utils/itemsController'
import { MenuHeader, DrawingTool } from './MenuHeader'
import { MenuBody } from './MenuBody'
import { PlacesModal } from 'components/PlacesModal'
import { EditBusStopModal } from 'components/EditBusStopModal'
import styles from './styles.module.scss'

interface MapEditorControllerProps {
  drawingTool: DrawingTool
  onDrawingToolClick: (tool: DrawingTool) => void
  onSaveClick: () => void
  onMapProcess: (routesCount: number) => void
  editableMenuItem: MenuItem | null
  polygonMenuItems: PolygonMenuItem[]
  busStopMenuItems: BusStopMenuItem[]
  toggleMenuItemEditable: (menuItem: MenuItem) => void
  updateBusStopMenuItem: () => void
  deletePolygonMenuItem: (deletedPolygonItem: PolygonMenuItem) => void
  deleteBusStopMenuItem: (deletedBusStopItem: BusStopMenuItem) => void
  isPlacesModalOpen: boolean
  setIsPlacesModalOpen: (isOpen: boolean) => void
  isEditBusStopModalOpen: boolean
  setIsEditBusStopModalOpen: (isOpen: boolean) => void
}

export function MapEditorController({
  drawingTool,
  onDrawingToolClick,
  onSaveClick,
  onMapProcess,
  editableMenuItem,
  polygonMenuItems,
  busStopMenuItems,
  toggleMenuItemEditable,
  updateBusStopMenuItem,
  deletePolygonMenuItem,
  deleteBusStopMenuItem,
  isPlacesModalOpen,
  setIsPlacesModalOpen,
  isEditBusStopModalOpen,
  setIsEditBusStopModalOpen,
}: MapEditorControllerProps) {
  /** Opens the places modal */
  const onOpenPlacesModal = (event: MouseEvent<HTMLElement>) => {
    event.stopPropagation()
    event.nativeEvent.stopImmediatePropagation()
    setIsPlacesModalOpen(true)
  }

  /** Opens the edit bus stop modal */
  const onOpenEditBusStopModal = (event: MouseEvent<HTMLElement>) => {
    event.stopPropagation()
    event.nativeEvent.stopImmediatePropagation()
    setIsEditBusStopModalOpen(true)
  }

  return (
    <aside className={styles.mapEditor}>
      <MenuHeader
        drawingTool={drawingTool}
        onDrawingToolClick={onDrawingToolClick}
        onSaveClick={onSaveClick}
        editableMenuItem={editableMenuItem}
      />

      <div className={styles.menuBodyContainer}>
        <MenuBody
          editableMenuItem={editableMenuItem}
          polygonMenuItems={polygonMenuItems}
          busStopMenuItems={busStopMenuItems}
          onMapProcess={onMapProcess}
          toggleMenuItemEditable={toggleMenuItemEditable}
          deletePolygonMenuItem={deletePolygonMenuItem}
          deleteBusStopMenuItem={deleteBusStopMenuItem}
          onOpenPlacesModal={onOpenPlacesModal}
          onOpenEditBusStopModal={onOpenEditBusStopModal}
        />
      </div>

      {editableMenuItem instanceof BusStopMenuItem && (
        <PlacesModal
          busStopMenuItem={editableMenuItem}
          isModalOpen={isPlacesModalOpen}
          setIsModalOpen={setIsPlacesModalOpen}
        />
      )}

      {editableMenuItem instanceof BusStopMenuItem && (
        <EditBusStopModal
          busStopMenuItem={editableMenuItem}
          updateBusStopMenuItem={updateBusStopMenuItem}
          isModalOpen={isEditBusStopModalOpen}
          setIsModalOpen={setIsEditBusStopModalOpen}
        />
      )}
    </aside>
  )
}
