import { useRouter } from 'next/router'
import { BiArrowBack as BackIcon } from 'react-icons/bi'
import { MdPanTool as PanIcon } from 'react-icons/md'
import { RiShapeFill as PolygonIcon } from 'react-icons/ri'
import { FaBus as BusIcon } from 'react-icons/fa'
import { FaSave as SaveIcon } from 'react-icons/fa'

import { MenuItem } from 'utils/itemsController'
import styles from './styles.module.scss'

export type DrawingTool = 'PAN' | 'POLYGON' | 'BUS_STOP'

interface MenuHeaderProps {
  drawingTool: DrawingTool
  onDrawingToolClick: (tool: DrawingTool) => void
  onSaveClick: () => void
  editableMenuItem: MenuItem | null
}

export function MenuHeader({
  drawingTool,
  onDrawingToolClick,
  onSaveClick,
}: MenuHeaderProps) {
  /** Control back button press */
  const router = useRouter()

  return (
    <header className={styles.menuHeader}>
      <button onClick={() => router.back()}>
        <BackIcon />
      </button>

      <span>
        <button
          onClick={() => onDrawingToolClick('PAN')}
          className={drawingTool === 'PAN' ? styles.btnActive : ''}
        >
          <PanIcon />
        </button>

        <button
          onClick={() => onDrawingToolClick('POLYGON')}
          className={drawingTool === 'POLYGON' ? styles.btnActive : ''}
        >
          <PolygonIcon />
        </button>

        <button
          onClick={() => onDrawingToolClick('BUS_STOP')}
          className={drawingTool === 'BUS_STOP' ? styles.btnActive : ''}
        >
          <BusIcon />
        </button>

        <button onClick={onSaveClick}>
          <SaveIcon />
        </button>
      </span>
    </header>
  )
}
