import { MouseEvent, useState } from 'react'
import { MdPlace as PlacesIcon } from 'react-icons/md'
import { MdModeEdit as EditIcon } from 'react-icons/md'
import { MdDelete as DeleteIcon } from 'react-icons/md'
import { FaRegDotCircle as CircleIcon } from 'react-icons/fa'

import {
  MenuItem,
  PolygonMenuItem,
  BusStopMenuItem,
} from 'utils/itemsController'
import styles from './styles.module.scss'

interface MenuBodyProps {
  editableMenuItem: MenuItem | null
  polygonMenuItems: PolygonMenuItem[]
  busStopMenuItems: BusStopMenuItem[]
  onMapProcess: (routesCount: number) => void
  toggleMenuItemEditable: (menuItem: MenuItem) => void
  deletePolygonMenuItem: (deletedPolygonItem: PolygonMenuItem) => void
  deleteBusStopMenuItem: (deletedBusStopItem: BusStopMenuItem) => void
  onOpenPlacesModal: (event: MouseEvent<HTMLElement>) => void
  onOpenEditBusStopModal: (event: MouseEvent<HTMLElement>) => void
}

export function MenuBody({
  editableMenuItem,
  polygonMenuItems,
  busStopMenuItems,
  onMapProcess,
  toggleMenuItemEditable,
  deletePolygonMenuItem,
  deleteBusStopMenuItem,
  onOpenPlacesModal,
  onOpenEditBusStopModal,
}: MenuBodyProps) {
  const [routeCount, setRouteCount] = useState('')
  const [circleVisible, setCircleVisible] = useState(false)

  const toggleCircleVisibility = () => {
    busStopMenuItems.forEach((busStopItem) => {
      busStopItem.circleVisible = !circleVisible
    })

    setCircleVisible(!circleVisible)
  }

  return (
    <div className={styles.menuBody}>
      <h4>Área do Mapa</h4>
      {polygonMenuItems.length > 0 ? (
        <ul>
          {polygonMenuItems.map((menuPolygon) => (
            <li
              key={menuPolygon.name}
              onMouseEnter={() => menuPolygon.onMouseEnter()}
              onMouseLeave={() => menuPolygon.onMouseLeave()}
              onClick={() => toggleMenuItemEditable(menuPolygon)}
              className={
                menuPolygon === editableMenuItem ? styles.itemActive : ''
              }
            >
              <span>{menuPolygon.name}</span>
              <span>
                <button
                  onClick={(event) => {
                    event.stopPropagation()
                    event.nativeEvent.stopImmediatePropagation()
                    deletePolygonMenuItem(menuPolygon)
                  }}
                >
                  <DeleteIcon />
                </button>
              </span>
            </li>
          ))}
        </ul>
      ) : (
        <p>(vazio)</p>
      )}

      <h4>Processar Mapa</h4>

      <input
        type="number"
        id="routeCount"
        placeholder="Número de rotas"
        value={routeCount}
        onChange={(event) => {
          setRouteCount(event.target.value)
        }}
      />

      <button
        type="button"
        onClick={() => onMapProcess(Number(routeCount))}
        className={styles.processButton}
      >
        Processar
      </button>

      <h4>
        <span>
          Pontos de Ônibus
          {busStopMenuItems.length > 0 ? ` (${busStopMenuItems.length})` : null}
        </span>

        <button
          type="button"
          onClick={toggleCircleVisibility}
          className={circleVisible ? styles.circleActive : ''}
        >
          <CircleIcon />
        </button>
      </h4>

      {busStopMenuItems.length > 0 ? (
        <ul>
          {busStopMenuItems.map((menuBusStop) => (
            <li
              key={menuBusStop.name}
              onMouseEnter={() => menuBusStop.onMouseEnter()}
              onMouseLeave={() => menuBusStop.onMouseLeave()}
              onClick={() => toggleMenuItemEditable(menuBusStop)}
              className={
                menuBusStop === editableMenuItem ? styles.itemActive : ''
              }
            >
              <span>{menuBusStop.name}</span>
              <span>
                <button onClick={onOpenPlacesModal}>
                  <PlacesIcon />
                </button>

                <button onClick={onOpenEditBusStopModal}>
                  <EditIcon />
                </button>

                <button
                  onClick={(event) => {
                    event.stopPropagation()
                    event.nativeEvent.stopImmediatePropagation()
                    deleteBusStopMenuItem(menuBusStop)
                  }}
                >
                  <DeleteIcon />
                </button>
              </span>
            </li>
          ))}
        </ul>
      ) : (
        <p>(vazio)</p>
      )}
    </div>
  )
}
