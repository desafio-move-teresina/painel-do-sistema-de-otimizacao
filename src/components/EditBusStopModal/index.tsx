import { useEffect } from 'react'
import Modal from 'react-modal'
import { SubmitHandler, useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup/dist/yup'
import * as yup from 'yup'
import { IoMdClose as CloseIcon } from 'react-icons/io'

import { BusStopMenuItem } from 'utils/itemsController'
import {
  defaultIntegerRule,
  defaultAngleRule,
  defaultBooleanRule,
} from 'utils/validation'
import styles from './styles.module.scss'

interface EditBusStopModalProps {
  busStopMenuItem: BusStopMenuItem
  updateBusStopMenuItem: () => void
  isModalOpen: boolean
  setIsModalOpen: (isModalOpen: boolean) => void
}

type EditBusStopFormData = {
  passengersIn: number
  passengersOut: number
  direction: number
  isActive: boolean
}

const editBusStopFormSchema = yup.object().shape({
  passengersIn: defaultIntegerRule,
  passengersOut: defaultIntegerRule,
  direction: defaultAngleRule,
  isActive: defaultBooleanRule,
})

export function EditBusStopModal({
  busStopMenuItem,
  updateBusStopMenuItem,
  isModalOpen,
  setIsModalOpen,
}: EditBusStopModalProps) {
  const { register, handleSubmit, formState, reset, setValue } =
    useForm<EditBusStopFormData>({
      resolver: yupResolver(editBusStopFormSchema),
    })

  const { errors } = formState

  useEffect(() => {
    const formStateClean = !formState.isDirty
    if (isModalOpen && formStateClean) {
      setValue('passengersIn', busStopMenuItem.passengersIn)
      setValue('passengersOut', busStopMenuItem.passengersOut)
      setValue('direction', busStopMenuItem.direction)
      setValue('isActive', busStopMenuItem.isActive)
    }
  }, [isModalOpen, formState.isDirty, setValue, busStopMenuItem])

  /** Close this modal */
  const onRequestClose = () => {
    reset()
    setIsModalOpen(false)
  }

  const onSubmit: SubmitHandler<EditBusStopFormData> = (values) => {
    busStopMenuItem.passengersIn = values.passengersIn
    busStopMenuItem.passengersOut = values.passengersOut
    busStopMenuItem.direction = values.direction
    busStopMenuItem.isActive = values.isActive

    // Also updating static members. New bus stops will be initialized
    // with the previous submitted value
    BusStopMenuItem.passengersIn = values.passengersIn
    BusStopMenuItem.passengersOut = values.passengersOut
    BusStopMenuItem.direction = values.direction
    BusStopMenuItem.isActive = values.isActive

    updateBusStopMenuItem()
    reset()
    onRequestClose()
  }

  return (
    <Modal
      isOpen={isModalOpen}
      onRequestClose={onRequestClose}
      overlayClassName="reactModalOverlay"
      className="reactModalContent"
    >
      <button
        type="button"
        onClick={onRequestClose}
        className="reactModalClose"
      >
        <CloseIcon />
      </button>

      <form
        onSubmit={handleSubmit(onSubmit)}
        className={styles.newBusStopModal}
      >
        <h2>Informações do ponto de ônibus</h2>

        <h3>Quantidade de passageiros neste ponto</h3>

        <fieldset>
          <label htmlFor="passengersIn">Entra no ônibus:</label>
          <input
            type="number"
            id="passengersIn"
            placeholder="Quantidade"
            {...register('passengersIn')}
          />
          <span>{errors.passengersIn?.message}</span>
        </fieldset>

        <fieldset>
          <label htmlFor="passengersOut">Desce do ônibus:</label>
          <input
            type="number"
            id="passengersOut"
            placeholder="Quantidade"
            {...register('passengersOut')}
          />
          <span>{errors.passengersOut?.message}</span>
        </fieldset>

        <h3>Outras informações</h3>

        <fieldset>
          <label htmlFor="direction">Direção da via (em graus):</label>
          <input
            type="number"
            id="direction"
            placeholder="Direção"
            {...register('direction')}
          />
          <span>{errors.direction?.message}</span>
        </fieldset>

        <div>
          <input
            type="checkbox"
            id="isActive"
            value={1}
            defaultChecked={busStopMenuItem.isActive}
            {...register('isActive')}
          />
          <label htmlFor="isActive">Ponto de ônibus ativo</label>
          <span>{errors.isActive?.message}</span>
        </div>

        <button type="submit">Ok</button>
      </form>
    </Modal>
  )
}
