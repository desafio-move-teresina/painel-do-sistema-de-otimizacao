import { createServer } from 'miragejs'

createServer({
  routes() {
    this.logging = false
    this.namespace = 'miragejs'

    this.get('/routes', () => {
      return [
        {
          id: 'abc111',
          name: 'NS-20',
          busStops: [
            {
              id: '311fb634-7e57-4701-afb6-a9ed8280b913',
              latitude: -5.0632544180518,
              longitude: -42.8195823069593,
              radius: 600,
              direction: 0,
              isActive: 1,
              passengersIn: 15,
              passengersOut: 15,
              places: [],
            },
            {
              id: '145de309-e71c-4962-b110-90f594916325',
              latitude: -5.0811227860051,
              longitude: -42.8133166667005,
              radius: 600,
              direction: 0,
              isActive: 1,
              passengersIn: 15,
              passengersOut: 15,
              places: [],
            },
          ],
          overviewPolyline:
            'hne^dyfdGaQhO{XdDqOyBkT{KiWiD_GaCy[`@ei@fZmFde@?zw@iItCsSkEy\\p@yYuP}IEih@`TeOfBmj@~BoEfBsBvK}VhMe[`Au]bC',
        },
        {
          id: 'abc222',
          name: 'LO-32',
          busStops: [],
          overviewPolyline: 'nnb^~midGsA_HGqCiC}LoC_GnDgp@YkSjDcP|Gc[kDcO',
        },
      ]
    })

    // Clearing namespace or passthrough won't work
    this.namespace = ''

    // Ignoring all next requests
    this.passthrough('/_next/**')

    // Ignoring all API requests
    this.passthrough(process.env.NEXT_PUBLIC_API_URL + '/**')
  },
})
